﻿namespace EIT_B19_v2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.unisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lekbolestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lekpakovanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvestajOBrojuLekovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poProizvodjacuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poGrupiLekovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unisToolStripMenuItem,
            this.izvestajOBrojuLekovaToolStripMenuItem,
            this.krajToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(510, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // unisToolStripMenuItem
            // 
            this.unisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lekbolestToolStripMenuItem,
            this.lekpakovanjeToolStripMenuItem});
            this.unisToolStripMenuItem.Name = "unisToolStripMenuItem";
            this.unisToolStripMenuItem.Size = new System.Drawing.Size(65, 29);
            this.unisToolStripMenuItem.Text = "Unos";
            this.unisToolStripMenuItem.Click += new System.EventHandler(this.unisToolStripMenuItem_Click);
            // 
            // lekbolestToolStripMenuItem
            // 
            this.lekbolestToolStripMenuItem.Name = "lekbolestToolStripMenuItem";
            this.lekbolestToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.lekbolestToolStripMenuItem.Text = "Lek-bolest";
            this.lekbolestToolStripMenuItem.Click += new System.EventHandler(this.lekbolestToolStripMenuItem_Click);
            // 
            // lekpakovanjeToolStripMenuItem
            // 
            this.lekpakovanjeToolStripMenuItem.Name = "lekpakovanjeToolStripMenuItem";
            this.lekpakovanjeToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.lekpakovanjeToolStripMenuItem.Text = "Lek-pakovanje";
            this.lekpakovanjeToolStripMenuItem.Click += new System.EventHandler(this.lekpakovanjeToolStripMenuItem_Click);
            // 
            // izvestajOBrojuLekovaToolStripMenuItem
            // 
            this.izvestajOBrojuLekovaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.poProizvodjacuToolStripMenuItem,
            this.poGrupiLekovaToolStripMenuItem});
            this.izvestajOBrojuLekovaToolStripMenuItem.Name = "izvestajOBrojuLekovaToolStripMenuItem";
            this.izvestajOBrojuLekovaToolStripMenuItem.Size = new System.Drawing.Size(201, 29);
            this.izvestajOBrojuLekovaToolStripMenuItem.Text = "Izvestaj o broju lekova";
            // 
            // poProizvodjacuToolStripMenuItem
            // 
            this.poProizvodjacuToolStripMenuItem.Name = "poProizvodjacuToolStripMenuItem";
            this.poProizvodjacuToolStripMenuItem.Size = new System.Drawing.Size(224, 30);
            this.poProizvodjacuToolStripMenuItem.Text = "Po proizvodjacu";
            this.poProizvodjacuToolStripMenuItem.Click += new System.EventHandler(this.poProizvodjacuToolStripMenuItem_Click);
            // 
            // poGrupiLekovaToolStripMenuItem
            // 
            this.poGrupiLekovaToolStripMenuItem.Name = "poGrupiLekovaToolStripMenuItem";
            this.poGrupiLekovaToolStripMenuItem.Size = new System.Drawing.Size(224, 30);
            this.poGrupiLekovaToolStripMenuItem.Text = "Po grupi lekova";
            this.poGrupiLekovaToolStripMenuItem.Click += new System.EventHandler(this.poGrupiLekovaToolStripMenuItem_Click);
            // 
            // krajToolStripMenuItem
            // 
            this.krajToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.izlazToolStripMenuItem});
            this.krajToolStripMenuItem.Name = "krajToolStripMenuItem";
            this.krajToolStripMenuItem.Size = new System.Drawing.Size(53, 29);
            this.krajToolStripMenuItem.Text = "Kraj";
            // 
            // izlazToolStripMenuItem
            // 
            this.izlazToolStripMenuItem.Name = "izlazToolStripMenuItem";
            this.izlazToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.I)));
            this.izlazToolStripMenuItem.Size = new System.Drawing.Size(182, 30);
            this.izlazToolStripMenuItem.Text = "Izlaz";
            this.izlazToolStripMenuItem.Click += new System.EventHandler(this.izlazToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 528);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem unisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lekbolestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lekpakovanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvestajOBrojuLekovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem poProizvodjacuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem poGrupiLekovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izlazToolStripMenuItem;
    }
}

