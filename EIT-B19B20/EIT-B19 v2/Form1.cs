﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EIT_B19_v2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void unisToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void lekbolestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lek_bolest form1 = new Lek_bolest();
            form1.ShowDialog();
        }

        private void poProizvodjacuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Po_proizvodjacu form2 = new Po_proizvodjacu();
            form2.ShowDialog();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lekpakovanjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Lek_pakovanje frrom = new Lek_pakovanje();
            frrom.ShowDialog();
        }

        private void poGrupiLekovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Po_grupi_lekova lekovi = new Po_grupi_lekova();
            lekovi.ShowDialog();
        }
    }
}
