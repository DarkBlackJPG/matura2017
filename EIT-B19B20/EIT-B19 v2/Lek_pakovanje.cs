﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace EIT_B19_v2
{
    public partial class Lek_pakovanje : Form
    {
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=evidencija_lekova_19;Integrated Security=True");
        public Lek_pakovanje()
        {
            InitializeComponent();
        }

        
        private void refreshSomeShit()
        {
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "SELECT lek.nazivLeka as [Naziv leka], proizvodjac.naziv as Proizvodjac, pakovanje.naziv as Pakovanje, pakovanje.kolicina as Kolicina From proizvodjac, lek, lek_pakovanje, pakovanje WHERE proizvodjac.proizvodjacID = lek.proizvodjacID AND lek.lekID = lek_pakovanje.lekID AND lek_pakovanje.pakovanjeID = pakovanje.pakovanjeID";
                _CONN.Open();
                SqlDataAdapter adr = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adr.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "SELECT lek.lekID, lek.nazivLeka FROM lek";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr[0].ToString() + " - " + rdr[1].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "select pakovanje.pakovanjeID, pakovanje.naziv from pakovanje";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr[0].ToString() + " - " + rdr[1].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
        private void Lek_pakovanje_Load(object sender, EventArgs e)
        {
            refreshSomeShit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li zelite da obrisete " + comboBox1.Text.Substring((comboBox1.Text.IndexOf('-') + 2)) + " sa " + comboBox2.Text.Substring((comboBox2.Text.IndexOf('-') + 2)) + "?", "Paznja", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    int lek = Convert.ToInt32(comboBox1.Text.Substring(0, comboBox1.Text.IndexOf('-')));
                    int bolest = Convert.ToInt32(comboBox2.Text.Substring(0, comboBox2.Text.IndexOf('-')));
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "DELETE FROM lek_pakovanje where lek_pakovanje.pakovanjeID = @bolest AND lek_pakovanje.lekID = @lek";
                    cmd.Parameters.Add("@lek", SqlDbType.Int).Value = lek.ToString();
                    cmd.Parameters.Add("@bolest", SqlDbType.Int).Value = bolest.ToString();
                    _CONN.Open();
                    if (cmd.ExecuteNonQuery() <= 0)
                    {
                        throw new Exception("Nema pronadjenih vrednosti");
                    }
                    MessageBox.Show("Uspesno obrisano.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();

                }
                refreshSomeShit();
            }
            else
                MessageBox.Show("Isis prekinut");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Paznja", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    int lek = Convert.ToInt32(comboBox1.Text.Substring(0, comboBox1.Text.IndexOf('-')));
                    int bolest = Convert.ToInt32(comboBox2.Text.Substring(0, comboBox2.Text.IndexOf('-')));
                    string napomena = textBox1.Text;
                    if (napomena != "")
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = _CONN;
                        cmd.CommandText = "INSERT INTO lek_pakovanje (pakovanjeID, lekID, komentar) VALUES ("+ bolest.ToString() +", "+lek.ToString()+", '"+napomena+"')";
                        //cmd.Parameters.Add("@lek", SqlDbType.Int).Value = lek.ToString();
                        //cmd.Parameters.Add("@bolest", SqlDbType.Int).Value = bolest.ToString();
                        //cmd.Parameters.Add("@napomena", SqlDbType.Text).Value = napomena;
                        _CONN.Open();
                        cmd.ExecuteNonQuery();
                        //label4.Text = cmd.CommandText;
                        MessageBox.Show("Uspesno upisano.");
                    }
                    else
                        MessageBox.Show("Bilo je problema sa Vasim unosom.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();

                }
                refreshSomeShit();
            }
            else
                MessageBox.Show("Upis prekinut");
        }
    }
}
