﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EIT_B19_v2
{
    public partial class Po_grupi_lekova : Form
    {
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=evidencija_lekova_19;Integrated Security=True");
        public Po_grupi_lekova()
        {
            InitializeComponent();
        }
        private void refreshStuff()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "select grupa_Leka.nazivGrupe from grupa_Leka";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    checkedListBox1.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void createSeries(string name)
        {

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select COUNT(*) as number from lek, grupa_Leka where grupa_Leka.nazivGrupe = @naziv and grupa_Leka.grupaLekaID = lek.grupaLekaID;";
                cmd.Parameters.Add("@naziv", SqlDbType.VarChar).Value = name;
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    //chart1.Series["Broj bolesti"].Points.AddY(Convert.ToInt32(rdr[0].ToString()));
                    chart1.Series["Grupa leka"].Points.AddXY(name, Convert.ToInt32(rdr[0].ToString()));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            List<string> items = new List<string>();
            foreach (string x in checkedListBox1.CheckedItems)
            {
                items.Add(x);
            }
            foreach (string item in items)
            {
                createSeries(item);
            }
        }

        private void Po_grupi_lekova_Load(object sender, EventArgs e)
        {
            refreshStuff();
        }
    }
}
