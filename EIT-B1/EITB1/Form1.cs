﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EITB1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void prijavaPasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Prijava_pasa p = new Prijava_pasa();
            p.ShowDialog();
        }

        private void spisakPasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Spisak_pasa s = new Spisak_pasa();
            s.ShowDialog();
        }
    }
}
