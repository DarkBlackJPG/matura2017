﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EITB1
{
    public partial class Spisak_pasa : Form
    {
        public Spisak_pasa()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Baza_pasa;Integrated Security=True");
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT PasID FROM PAS", conn);
                SqlDataReader read = cmd.ExecuteReader();
                int i = 0;
                while(read.Read())
                {
                    ++i;
                }
                //racunam koliko je ukupno pasa upisano u bazi podataka
                label2.Text = "Ukupan broj pasa koji su prijavljeni: " + i.ToString();
                read.Close();
                string s = comboBox1.Text;
               //racunam koliko ima pasa u odredjenoj kategoriji
                SqlCommand cmd2 = new SqlCommand("SELECT COUNT(Pas.PasID),Kategorija.Naziv,Kategorija.KategorijaID FROM Pas,Kategorija,Izlozba,Rezultat WHERE Pas.PasID = Rezultat.PasID AND Rezultat.KategorijaID=Kategorija.KategorijaID AND Rezultat.IzlozbaID = Izlozba.IzlozbaID AND Izlozba.IzlozbaID=" + s + "GROUP BY Kategorija.Naziv, Kategorija.KategorijaID", conn);
                read = cmd2.ExecuteReader();
                
                DataTable dt = new DataTable();
                if(dt.Columns.Count==0)
                {
                dt.Columns.Add("Sifra");
                dt.Columns.Add("Naziv kategorije");
                dt.Columns.Add("Broj pasa");
                }
                string k = "";
                string b = "";
                string c = "";
                int a = 0;
                while(read.Read())
                {
                    
                    k = read[0].ToString();
                    b = read[1].ToString();
                    c = read[2].ToString();
                    a += Convert.ToInt32(k);
                    dt.Rows.Add(c, b, k);
                }
                // u k se upisuje koliko ima pasa po kategoriji, to sve saberem u a, nisam siguran da li se ovo zahtevalo od mene
                label3.Text = "Ukupan broj pasa koji se takmicio: " + a.ToString();
                dataGridView1.DataSource = dt;
                read.Close();
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void Spisak_pasa_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Baza_pasa;Integrated Security=True");
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT IzlozbaID FROM Izlozba ORDER BY IzlozbaID", conn);
                SqlDataReader read = cmd.ExecuteReader();
                
                while (read.Read())
                {
                    comboBox1.Items.Add(read[0].ToString());
                }
                read.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
