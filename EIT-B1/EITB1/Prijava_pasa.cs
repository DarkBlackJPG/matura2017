﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EITB1
{
    public partial class Prijava_pasa : Form
    {
        
        public Prijava_pasa()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Prijava_pasa_Load(object sender, EventArgs e)
        {
            //ucitavanje podataka iz baze iz tabela Kategorija, Pas, Izlozba
            
            //kategorija
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Baza_pasa;Integrated Security=True");
            
            try
            {
                conn.Open();
            
            SqlCommand kategorija = new SqlCommand("SELECT KategorijaID, Naziv FROM Kategorija ORDER BY KategorijaID",conn);
            SqlDataReader read = kategorija.ExecuteReader();
            
            while(read.Read())
            {
                comboBox3.Items.Add(read[0].ToString() + " - " + read[1].ToString());
                
            }
            read.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
            finally
            {
                conn.Close();
            }

            //izlozba
            
            try
            {
            
                conn.Open();
                SqlCommand izlozba = new SqlCommand("SELECT IzlozbaID, Mesto FROM Izlozba ORDER BY IzlozbaID",conn);

            SqlDataReader read = izlozba.ExecuteReader();
            
            while (read.Read())
            {
                comboBox2.Items.Add(read[0].ToString() + " - " + read[1].ToString());
                
            }
            read.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                
            }

            //pas

            try
            {

                conn.Open();
                
            SqlCommand pas = new SqlCommand("SELECT PasID, Ime FROM Pas ORDER BY PasID",conn);
            SqlDataReader read = pas.ExecuteReader();
            
            while (read.Read())
            {
                comboBox1.Items.Add(read[0].ToString() + " - " + read[1].ToString());
                
            }
            read.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if ((comboBox1.Text != "") && (comboBox2.Text != "") && (comboBox3.Text != ""))
            {
                SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Baza_pasa;Integrated Security=True");
                SqlConnection conn2 = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Baza_pasa;Integrated Security=True");
                try
                {
                    string s = comboBox1.Text;
                    string[] s1 = comboBox2.Text.Split('-');
                    string[] s2 = comboBox3.Text.Split('-');
                    conn.Open();
                    conn2.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO Pas (Ime) Values ('" + s +"')",conn);
                    cmd.ExecuteNonQuery();
                    SqlCommand cmd3 = new SqlCommand("SELECT TOP 1 PasID FROM Pas ORDER BY PasID DESC", conn);//ucita poslednji upisan ID
                    SqlDataReader rd = cmd3.ExecuteReader();
                    rd.Read();
                    //Identity_Insert je na OFF po defaultu
                    //nije bilo potrebno 2 konekcije, dovoljna je jedna
                    SqlCommand cmd2 = new SqlCommand("SET IDENTITY_INSERT Rezultat ON;INSERT INTO Rezultat (PasID, IzlozbaID, KategorijaID) Values('" + rd[0].ToString() + "', '" + s1[0].Trim() + "', '" + s2[0].Trim() + "');SET IDENTITY_INSERT Rezultat OFF", conn2);
                    cmd2.ExecuteNonQuery();
                    comboBox1.Items.Add(rd[0].ToString() + " - " + s);
                    comboBox1.Text = String.Empty;
                    MessageBox.Show("Pisanje je uspesno izvrseno");
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                    conn2.Close();
                }
            }
            else
            {
                MessageBox.Show("Unesi podatke u sva polja");
            }


        }    
            
            
        

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button2.Enabled = false;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
        }

        private void radioButton2_Click(object sender, EventArgs e)
        {
            button2.Enabled = true;
            button1.Enabled = false;
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Baza_pasa;Integrated Security=True");
            try
            {
                string a = comboBox1.Text;
                string[] s = comboBox1.Text.Split('-');
                conn.Open();
                SqlCommand cmd = new SqlCommand("DELETE FROM Rezultat WHERE PasID=" + s[0].TrimEnd(), conn);
                SqlCommand cmd2 = new SqlCommand("DELETE FROM Pas WHERE PasID=" + s[0].TrimEnd(), conn);
                cmd.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();
                comboBox1.Items.Remove(a);
                comboBox1.Text = String.Empty;
                MessageBox.Show("Brisanje je uspesno izvrseno");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
