﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FUDBALSKIKLUB_maturaprogramiranje
{
    public partial class FK : Form
    {
        public FK()
        {
            InitializeComponent();
        }

        private void krajRadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void unosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Gradovi gr = new Gradovi();
            gr.ShowDialog();
            
        }

        private void spisakIgracaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Spisak sp = new Spisak();
        
            sp.ShowDialog();
        }

        private void unosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Stadion st = new Stadion();
            st.ShowDialog();
        }

        private void kapacitetStadionaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Kapacitet ka = new Kapacitet();
            ka.ShowDialog();
        }
    }
}
