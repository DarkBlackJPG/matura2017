﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace FUDBALSKIKLUB_maturaprogramiranje
{
    public partial class Gradovi : Form
    {
        SqlConnection konekcija = new SqlConnection();
        int b=1;
        public Gradovi()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox1.Focus();
            b = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int poz = 0 ;
            int x = 0;
            
            SqlCommand komanda = new SqlCommand("select * from Grad", konekcija);
            konekcija.Open();
            SqlDataReader citac = komanda.ExecuteReader();
          
            while (citac.Read())
            {
                x++;
                
            }
           
         
            konekcija.Close();
            SqlCommand komanda2 = new SqlCommand("select * from Grad", konekcija);
            konekcija.Open();
            SqlDataReader r = komanda2.ExecuteReader();

            while (r.Read())
            {
                poz++;
                if (textBox1.Text == "")
                {
                    textBox1.Text = r["gradid"].ToString();
                    textBox2.Text = r["grad"].ToString();
                    textBox3.Text = r["pozivnibroj"].ToString();
                    textBox4.Text = r["postanskibroj"].ToString();
                    textBox5.Text = r["brojstanovnika"].ToString();
                    break;
                }
                else
                {
                    if (b==1)
                    {
                        if (poz==x)
                        {
                            textBox1.Text = r["gradid"].ToString();
                            textBox2.Text = r["grad"].ToString();
                            textBox3.Text = r["pozivnibroj"].ToString();
                            textBox4.Text = r["postanskibroj"].ToString();
                            textBox5.Text = r["brojstanovnika"].ToString();
                            b = x;
                        }
                    }
                    else
                    {
                        if (poz==b-1)
                        {
                            textBox1.Text = r["gradid"].ToString();
                            textBox2.Text = r["grad"].ToString();
                            textBox3.Text = r["pozivnibroj"].ToString();
                            textBox4.Text = r["postanskibroj"].ToString();
                            textBox5.Text = r["brojstanovnika"].ToString();
                            b--;
                            break;
                        }
                    }
                }
            }
            konekcija.Close();
        }

        private void Gradovi_Load(object sender, EventArgs e)
        {
            konekcija.ConnectionString = @"Data Source=puzovic-pc\sqlexpress;Initial Catalog=fudbalskiklub;Integrated Security=True";
            SqlCommand komanda = new SqlCommand("select * from Grad", konekcija);
            konekcija.Open();
            SqlDataReader citac = komanda.ExecuteReader();

            while (citac.Read())
            {


                textBox1.Text = citac["gradid"].ToString();
                textBox2.Text = citac["grad"].ToString();
                textBox3.Text = citac["pozivnibroj"].ToString();
                textBox4.Text = citac["postanskibroj"].ToString();
                textBox5.Text = citac["brojstanovnika"].ToString();
                break;
            }

            b = 1;
            konekcija.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int poz = 0;
            int x = 0;
            SqlCommand komanda = new SqlCommand("select * from Grad", konekcija);
            konekcija.Open();
            SqlDataReader citac = komanda.ExecuteReader();

            while (citac.Read())
            {
                x++;

            }
           


            konekcija.Close();
            SqlCommand komanda2 = new SqlCommand("select * from Grad", konekcija);
            konekcija.Open();
            SqlDataReader r = komanda2.ExecuteReader();

            while (r.Read())
            {
                poz++;
                if (textBox1.Text == "")
                {
                    if (poz==x)
                    {
                        textBox1.Text = r["gradid"].ToString();
                        textBox2.Text = r["grad"].ToString();
                        textBox3.Text = r["pozivnibroj"].ToString();
                        textBox4.Text = r["postanskibroj"].ToString();
                        textBox5.Text = r["brojstanovnika"].ToString();
                        b = x;
                    }  
                }
                else
                {
                    if (b==x)
                    {
                        if (poz == 1)
                        {
                            textBox1.Text = r["gradid"].ToString();
                            textBox2.Text = r["grad"].ToString();
                            textBox3.Text = r["pozivnibroj"].ToString();
                            textBox4.Text = r["postanskibroj"].ToString();
                            textBox5.Text = r["brojstanovnika"].ToString();
                            b = 1;
                            break;
                        }
                    }
                    else
                    {
                        if (poz == b+1)
                        {
                            textBox1.Text = r["gradid"].ToString();
                            textBox2.Text = r["grad"].ToString();
                            textBox3.Text = r["pozivnibroj"].ToString();
                            textBox4.Text = r["postanskibroj"].ToString();
                            textBox5.Text = r["brojstanovnika"].ToString();
                            b ++;
                            break;
                        }
                    }
                }
            }
            konekcija.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                SqlCommand komanda2 = new SqlCommand("insert into Grad  values('" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "'," + textBox5.Text + ")", konekcija);
                konekcija.Open();
                komanda2.ExecuteNonQuery();
                konekcija.Close();
              
                MessageBox.Show("USPESNO UNESENO");
            }
            else
            {
                MessageBox.Show("UNESITE SVE VREDNOSTI");
            }
        }
    }
}
