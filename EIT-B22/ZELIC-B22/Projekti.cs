﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ZELIC_B22
{
    public partial class Projekti : Form
    {
        
       
        public Projekti()
        {
            InitializeComponent();
        }

        private void izvestajuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Projekti_Load(object sender, EventArgs e)
        {

        }

        private void radnaMestaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void radniciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Radnici radnici = new Radnici();
            radnici.ShowDialog();
        }

        private void analizaBudzetaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Analiza_budzeta analiza_budzeta = new Analiza_budzeta();
            analiza_budzeta.ShowDialog();
        }
    }
}
