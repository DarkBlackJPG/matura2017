﻿namespace ZELIC_B22
{
    partial class Projekti
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.unosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radnaMestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvestajuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nezavrseniProjektiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaBudzetaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krajRadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unosToolStripMenuItem,
            this.izvestajuToolStripMenuItem,
            this.krajRadaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // unosToolStripMenuItem
            // 
            this.unosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.radnaMestaToolStripMenuItem,
            this.radniciToolStripMenuItem});
            this.unosToolStripMenuItem.Name = "unosToolStripMenuItem";
            this.unosToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.unosToolStripMenuItem.Text = "Unos";
            // 
            // radnaMestaToolStripMenuItem
            // 
            this.radnaMestaToolStripMenuItem.Name = "radnaMestaToolStripMenuItem";
            this.radnaMestaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.radnaMestaToolStripMenuItem.Text = "Radna mesta";
            this.radnaMestaToolStripMenuItem.Click += new System.EventHandler(this.radnaMestaToolStripMenuItem_Click);
            // 
            // radniciToolStripMenuItem
            // 
            this.radniciToolStripMenuItem.Name = "radniciToolStripMenuItem";
            this.radniciToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.radniciToolStripMenuItem.Text = "Radnici";
            this.radniciToolStripMenuItem.Click += new System.EventHandler(this.radniciToolStripMenuItem_Click);
            // 
            // izvestajuToolStripMenuItem
            // 
            this.izvestajuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nezavrseniProjektiToolStripMenuItem,
            this.analizaBudzetaToolStripMenuItem});
            this.izvestajuToolStripMenuItem.Name = "izvestajuToolStripMenuItem";
            this.izvestajuToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.izvestajuToolStripMenuItem.Text = "Izvestaji";
            this.izvestajuToolStripMenuItem.Click += new System.EventHandler(this.izvestajuToolStripMenuItem_Click);
            // 
            // nezavrseniProjektiToolStripMenuItem
            // 
            this.nezavrseniProjektiToolStripMenuItem.Name = "nezavrseniProjektiToolStripMenuItem";
            this.nezavrseniProjektiToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.nezavrseniProjektiToolStripMenuItem.Text = "Nezavrseni projekti";
            // 
            // analizaBudzetaToolStripMenuItem
            // 
            this.analizaBudzetaToolStripMenuItem.Name = "analizaBudzetaToolStripMenuItem";
            this.analizaBudzetaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.analizaBudzetaToolStripMenuItem.Text = "Analiza budzeta";
            this.analizaBudzetaToolStripMenuItem.Click += new System.EventHandler(this.analizaBudzetaToolStripMenuItem_Click);
            // 
            // krajRadaToolStripMenuItem
            // 
            this.krajRadaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.izlazToolStripMenuItem});
            this.krajRadaToolStripMenuItem.Name = "krajRadaToolStripMenuItem";
            this.krajRadaToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.krajRadaToolStripMenuItem.Text = "Kraj rada";
            // 
            // izlazToolStripMenuItem
            // 
            this.izlazToolStripMenuItem.Name = "izlazToolStripMenuItem";
            this.izlazToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.I)));
            this.izlazToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.izlazToolStripMenuItem.Text = "Izlaz";
            this.izlazToolStripMenuItem.Click += new System.EventHandler(this.izlazToolStripMenuItem_Click);
            // 
            // Projekti
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Projekti";
            this.Text = "Projekti";
            this.Load += new System.EventHandler(this.Projekti_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem unosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvestajuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem radnaMestaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem radniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nezavrseniProjektiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaBudzetaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krajRadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izlazToolStripMenuItem;
    }
}

