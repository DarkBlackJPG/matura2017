﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ZELIC_B22
{
    public partial class Radnici : Form
    {
        SqlConnection con = new SqlConnection(@"Data Source=ZELIC\SQLEXPRESS;Initial Catalog=B22;Integrated Security=True");
        public Radnici()
        {
            InitializeComponent();
        }
      

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (radioButton1.Checked)
            {
                textBox2.Enabled = textBox3.Enabled = textBox4.Enabled = textBox5.Enabled = comboBox1.Enabled = comboBox2.Enabled = comboBox3.Enabled = comboBox4.Enabled = comboBox5.Enabled = true;
                string poruka="Uspesno upisano";
              string ime = textBox3.Text;
                string prezime = textBox2.Text;
                string rodjenje = comboBox1.Text + "." + comboBox2.Text + "." + textBox4.Text+".";
                string zaposlenja = comboBox3.Text + "." + comboBox4.Text + "." + textBox5.Text+".";
                string kvalifikacija = comboBox5.Text;
                try{
                    SqlCommand com = new SqlCommand("INSERT INTO Radnik(Ime,Prezime,DatumRodjenja,DatumZaposlenja,KvalifikacijaID) VALUES('" + ime + "','" + prezime + "','" + rodjenje + "','" + zaposlenja + "','" + kvalifikacija + "')", con); //jbg ovi podaci za kancelariju i sektor ce ostati null/prazni, mada moze se i tako odraditi da tabela radnici ostane kao i na slici iz zadatka, a da radnik-kancelarija bude posebna tabela
                con.Open();
                com.ExecuteNonQuery();
               // Radnici_Load(this, null);
                }
                catch{poruka="Pogresno unesene vrednosti, pokusajte ponovo";}
                finally { MessageBox.Show(poruka); con.Close(); }
            }
            if (radioButton2.Checked)
            {
                int x = Convert.ToInt16(textBox1.Text);
                SqlCommand del = new SqlCommand("DELETE FROM Radnik WHERE RadnikID="+x,con);
                try
                {
                    con.Open();
                    del.ExecuteNonQuery();
                    MessageBox.Show("USPESNO OBRISAN RADNIK BROJ " + x);
                    con.Close();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); con.Close(); }
            }
            if (radioButton3.Checked)
            {
                textBox2.Enabled = textBox3.Enabled = textBox4.Enabled = textBox5.Enabled = comboBox1.Enabled = comboBox2.Enabled = comboBox3.Enabled = comboBox4.Enabled = comboBox5.Enabled = true;
                SqlCommand com =new SqlCommand("UPDATE Radnik SET Ime='"+textBox3.Text+"',Prezime='"+textBox2.Text+"',DatumRodjenja='"+comboBox1.Text + "." + comboBox2.Text + "." + textBox4.Text+"."+"',DatumZaposlenja='"+comboBox3.Text + "." + comboBox4.Text + "." + textBox5.Text+"."+"',KvalifikacijaID='"+comboBox5.Text+"' WHERE RadnikID="+Convert.ToInt16(textBox1.Text),con);
                try
                {
                    con.Open();
                    com.ExecuteNonQuery();
                    MessageBox.Show("USPESNO IZMENJEN RADNIK BROJ " +textBox1.Text);
                    con.Close();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); con.Close(); }
                
            }
        }

        private void Radnici_Load(object sender, EventArgs e)
        {
           /* SqlConnection con = new SqlConnection(@"Data Source=ZELIC\SQLEXPRESS;Initial Catalog=B22;Integrated Security=True");
            SqlCommand com = new SqlCommand("SELECT COUNT(*) FROM Radnik",con);
            con.Open();
            int ukupno = Convert.ToInt16(com.ExecuteScalar());
            con.Close();
            comboBox6.Items.Clear();
            for (int i = 1; i <= ukupno; i++)
            {
                comboBox6.Items.Add(i);
            }
            comboBox6.MaxDropDownItems = ukupno;*/                            //JEBENO SAM SLUCAJNO GLEDAO SLIKU ZA B21 ZADATAK
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked || radioButton3.Checked)
            {
                if (textBox1.Text != "")
                {
                    try
                    {
                        int broj = Convert.ToInt32(textBox1.Text);
                        SqlCommand com = new SqlCommand("SELECT * FROM Radnik WHERE Radnik.RadnikID="+Convert.ToInt16(textBox1.Text), con);
                        con.Open();
                        SqlDataReader rdr = com.ExecuteReader();
                        while (rdr.Read())
                        {
                            textBox2.Text = rdr["Prezime"].ToString();
                            textBox3.Text = rdr["Ime"].ToString();
                            comboBox5.Text = rdr["KvalifikacijaID"].ToString();
                            string rodj = rdr["DatumRodjenja"].ToString();
                            string[] rodjenje = rodj.Split('.');
                            string zapos = rdr["DatumZaposlenja"].ToString();
                            string[] zaposlenje = zapos.Split('.');
                            comboBox3.Text = zaposlenje[0];
                            comboBox4.Text=zaposlenje[1]  ;
                            textBox5.Text = zaposlenje[2];
                            comboBox1.Text = rodjenje[0];
                            comboBox2.Text =rodjenje[1];
                            textBox4.Text = rodjenje[2];
                        }
                        con.Close();
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); con.Close(); }
                }
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
           
                textBox1.Text = "1";
                textBox2.Enabled = textBox3.Enabled = textBox4.Enabled = textBox5.Enabled = comboBox1.Enabled = comboBox2.Enabled = comboBox3.Enabled = comboBox4.Enabled = comboBox5.Enabled = false;

            
        }
    }
}
