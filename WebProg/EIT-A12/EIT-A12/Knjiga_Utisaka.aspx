﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Knjiga_Utisaka.aspx.cs" Inherits="EIT_A12.Uputstvo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #TextArea1 {
            height: 236px;
            width: 380px;
        }
    </style>
    <link href="main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a href="OAutoru.aspx">O Autoru</a> &nbsp;<a href="Uputstvo.aspx">Uputstvo.aspx</a>
        <h1>Knjiga utisaka</h1>
        <p>
            <asp:Label ID="Label1" runat="server" Text="Ime: "></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server" required></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label2" runat="server" Text="Email: "></asp:Label>
            <asp:TextBox ID="TextBox2" TextMode="Email" runat="server" required></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label3" runat="server" Text="Komentar: "></asp:Label>
            <asp:TextBox ID="TextBox3" runat="server" Height="228px" TextMode="MultiLine" Width="353px"></asp:TextBox>
        </p>
        <p>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        </p>
        <p>
            &nbsp;</p>
        <p>
            <asp:Label ID="Label4" runat="server"></asp:Label>
        </p>

    </div>
    </form>
</body>
</html>
