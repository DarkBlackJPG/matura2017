﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Sql;

namespace EIT_A12
{
    public partial class Uputstvo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-A12;Integrated Security=True");
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Insert into Utisak(ime, email, komentar, datum) values(@ime, @email, @komentar, @datum)";
                cmd.Parameters.Add("@ime", System.Data.SqlDbType.VarChar).Value = TextBox1.Text;
                cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar).Value = TextBox2.Text;
                cmd.Parameters.Add("@komentar", System.Data.SqlDbType.VarChar).Value = TextBox3.Text;
                cmd.Parameters.Add("@datum", System.Data.SqlDbType.Date).Value = Convert.ToString(DateTime.Now.ToShortDateString());
                _CONN.Open();
                if (cmd.ExecuteNonQuery() >= 1)
                {
                    Label4.Text = "Uspesno dodato";
                }
                else
                    Label4.Text = "Doslo je do nekih problema";
            }
            catch(Exception ex)
            {
                Label4.Text = ex.Message;
            }
            finally
            {
                _CONN.Close();
            }
        }
    }
}