﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EIT_A10.localhost;

namespace EIT_A10
{
    public partial class azuriranjeListe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                prviServis srvce = new prviServis();
                string[] array = srvce.ProcitajSveValute();
                string[] newArray = array.Distinct().ToArray();
                foreach (string x in newArray)
                {
                    DropDownList1.Items.Add(x);
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            prviServis srvcr = new prviServis();

            string date = Calendar1.SelectedDate.Year + "-" + Calendar1.SelectedDate.Month + "-" + Calendar1.SelectedDate.Day;
            double vals = 0;
            double.TryParse(TextBox1.Text, out vals);

            srvcr.boolUpisiKursNaDan(Convert.ToDateTime(date), DropDownList1.Text.Trim(' '), vals);
        }
    }
}