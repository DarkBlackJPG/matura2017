﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EIT_A13_NonMVC_
{
    class Element
    {
        string author, albumName, ganre, recordLabel, releaseString, picture;
        public string Author
        {
            get { return author; }
            set { author = value; }
        }
        public string AlbumName
        {
            get { return albumName; }
            set { albumName = value; }
        }
        public string Ganre
        {
            get { return ganre; }
            set { ganre = value; }
        }
        public string RecordLabel
        {
            get { return recordLabel; }
            set { recordLabel = value; }
        }
        public string ReleaseDate
        {
            get { return releaseString; }
            set { releaseString = value; }
        }
        public string Adress
        {
            get { return picture; }
            set { picture = value; }
        }

        public void insertDataFromFileToElement(string elementString)
        {
            string[] elementArray = elementString.Split('|');
            author = elementArray[0];
            albumName = elementArray[1];
            ganre = elementArray[2];
            releaseString = elementArray[3];
            recordLabel = elementArray[4];
            picture = elementArray[5];


        }
    }
}