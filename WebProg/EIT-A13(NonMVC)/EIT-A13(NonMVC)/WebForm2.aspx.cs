﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace EIT_A13_NonMVC_
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        List<Element> myArray = new List<Element>();
        protected void Page_Load(object sender, EventArgs e)
        {
            StreamReader rdr = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\WebProg\EIT-A13(NonMVC)\EIT-A13(NonMVC)\katalog.txt");
            List<string> elementsInFile = new List<string>();
            string readerString = rdr.ReadToEnd();
            string[] elementArray = readerString.Split('\n');
            foreach (string element in elementArray)
            {
                Element elementX = new Element();
                elementX.insertDataFromFileToElement(element);
                myArray.Add(elementX);
            }

            rdr.Close();
            if (!IsPostBack)
            {
                
                
                List<string> ganres = new List<string>();
                List<string> releaseDates = new List<string>();

                IEnumerable<string> ganresDistinct = ganres;
                IEnumerable<string> releaseDatesDistinct = releaseDates;

                foreach (Element elementX in myArray)
                {
                    ganres.Add(elementX.Ganre);
                    releaseDates.Add(elementX.ReleaseDate);
                }

                foreach (string elementX in ganresDistinct.Distinct())
                {
                    DropDownList1.Items.Add(elementX);

                }
                foreach (string elementX in releaseDatesDistinct.Distinct())
                {
                    DropDownList2.Items.Add(elementX);
                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<Element> newElementArray = new List<Element>();
            
            string author = "";
            string album = "";
            string ganre = "";
            string year = "";
            string recordLabel = ""; 
            foreach(Element x in myArray)
            {
                if(x.Author.Contains(TextBox1.Text) && x.AlbumName.Contains(TextBox2.Text) && x.Ganre.Contains(DropDownList1.Text) && x.ReleaseDate.Contains(DropDownList2.Text) && x.RecordLabel.Contains(TextBox3.Text))
                {
                    newElementArray.Add(x);
                }
            }

            foreach (Element x in newElementArray)
            {
                TableRow tr = new TableRow();
                TableCell tc1 = new TableCell();
                TableCell tc2= new TableCell();
                TableCell tc3 = new TableCell();
                TableCell tc4 = new TableCell();
                TableCell tc5 = new TableCell();
                TableCell tc6 = new TableCell();
                tc1.Text = x.Author;
                tc2.Text = x.AlbumName;
                tc3.Text = x.Ganre;
                tc4.Text = x.ReleaseDate;
                tc5.Text = x.RecordLabel;
                tc6.Text = String.Format("<img src='{0}'/>", x.Adress);
                tr.Cells.Add(tc1);
                tr.Cells.Add(tc2);
                tr.Cells.Add(tc3);
                tr.Cells.Add(tc4);
                tr.Cells.Add(tc5);
                tr.Cells.Add(tc6);

                Table1.Rows.Add(tr);
            }
            
            


        }
    }
}