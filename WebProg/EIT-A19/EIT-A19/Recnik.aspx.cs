﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace EIT_A19
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-A19;Integrated Security=True");
        protected void Button1_Click(object sender, EventArgs e)
        {
            if(DropDownList1.Text == "Englesko-Srpski")
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select * from Recnik where Engleski = '"+TextBox1.Text.ToLower()+"'";
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if(rdr.HasRows)
                    {
                        while(rdr.Read())
                        {
                            TextBox2.Text = rdr[2].ToString();
                            TextBox3.Text = rdr[3].ToString();
                        }
                    }
                    else
                    {
                        TextBox2.Text = "Ne postoji trazena rec!";
                        TextBox3.Text = "Ne postoji trazena rec!";
                    }
                    
                }
                catch (Exception ex)
                {
                    Label2.Text = ex.Message;
                }
                finally
                {
                    _CONN.Close();
                }
            }
            else if(DropDownList1.Text == "Srpsko-Engleski")
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select * from Recnik where Srpski = '" + TextBox1.Text.ToLower() + "'";
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            TextBox2.Text = rdr[1].ToString();
                            TextBox3.Text = rdr[3].ToString();
                        }
                    }
                    else
                    {
                        TextBox2.Text = "Ne postoji trazena rec!";
                        TextBox3.Text = "Ne postoji trazena rec!";
                    }

                }
                catch (Exception ex)
                {
                    Label2.Text = ex.Message;
                }
                finally
                {
                    _CONN.Close();
                }
            }
        }
    }
}