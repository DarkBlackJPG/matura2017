﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data.Sql;

namespace EIT_A19
{
    public partial class UnosReci : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-A19;Integrated Security=True");
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Insert into Recnik(Engleski, Srpski, Opis) values(@eng, @srp, @ops)";
                cmd.Parameters.AddWithValue("@eng", TextBox2.Text.ToLower());
                cmd.Parameters.AddWithValue("@srp", TextBox1.Text.ToLower());
                cmd.Parameters.AddWithValue("@ops", TextBox3.Text);
                _CONN.Open();
                if (cmd.ExecuteNonQuery() >= 1)
                    Label4.Text = "Uspesno Dodato!";
            }
            catch(Exception ex)
            {
                Label4.Text = ex.Message;
            }
            finally
            {
                _CONN.Close();
            }
        }
    }
}