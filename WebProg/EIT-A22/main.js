var isSortDesc = false

function sortTable() {

	if(isSortDesc == false)
	{
		var rbrOldArray = [];
		var imeOldArray = [];
		var prezimeOldArray = [];
		var skolaOldArray = [];
		var poeniOldArray = [];

		var rbrNewArray = [];
		var imeNewArray = [];
		var prezimeNewArray = [];
		var skolaNewArray = [];
		var poeniNewArray = [];

		for (var i = 0; i < document.getElementById('myTable').getElementsByTagName('tr').length; i++) {
			
			rbrOldArray.push(document.getElementById('myTable').rows[i].cells[0].innerHTML);
			imeOldArray.push(document.getElementById('myTable').rows[i].cells[1].innerHTML);
			prezimeOldArray.push(document.getElementById('myTable').rows[i].cells[2].innerHTML);
			skolaOldArray.push(document.getElementById('myTable').rows[i].cells[3].innerHTML);
			poeniOldArray.push(document.getElementById('myTable').rows[i].cells[4].innerHTML);

			rbrNewArray.push(document.getElementById('myTable').rows[i].cells[0].innerHTML);
			imeNewArray.push(document.getElementById('myTable').rows[i].cells[1].innerHTML);
			prezimeNewArray.push(document.getElementById('myTable').rows[i].cells[2].innerHTML);
			skolaNewArray.push(document.getElementById('myTable').rows[i].cells[3].innerHTML);
			poeniNewArray.push(document.getElementById('myTable').rows[i].cells[4].innerHTML);

		}

		poeniNewArray.sort();
		poeniNewArray.reverse();
		isSortDesc = true;

		for(var i = 0; i < poeniNewArray.length; i++)
		{
			for(var j = 0; j < poeniOldArray.length; j++)
			{
				if(poeniNewArray[i] == poeniOldArray[j])
				{
					imeNewArray[i] = imeOldArray[j];
					prezimeNewArray[i] = prezimeOldArray[j];
					skolaNewArray[i] = skolaOldArray[j];
					rbrNewArray[i] = rbrOldArray[j];
				}
			}
		}


		for (var i = 0; i < document.getElementById('myTable').getElementsByTagName('tr').length; i++) {
			
			document.getElementById('myTable').rows[i].cells[0].innerHTML = rbrNewArray[i];
			document.getElementById('myTable').rows[i].cells[1].innerHTML = imeNewArray[i];
			document.getElementById('myTable').rows[i].cells[2].innerHTML = prezimeNewArray[i];
			document.getElementById('myTable').rows[i].cells[3].innerHTML = skolaNewArray[i];
			document.getElementById('myTable').rows[i].cells[4].innerHTML = poeniNewArray[i];

		}

	}
}

function colorCells()
{

	for(var i = 1; i < document.getElementById('myTable').getElementsByTagName('tr').length; i++)
	{
		if((i%2) != 0)
		{
			document.getElementById('myTable').rows[i].cells[0].style.backgroundColor = 'lightgray';
			document.getElementById('myTable').rows[i].cells[1].style.backgroundColor = 'lightgray';
			document.getElementById('myTable').rows[i].cells[2].style.backgroundColor = 'lightgray';
			document.getElementById('myTable').rows[i].cells[3].style.backgroundColor = 'lightgray';
			document.getElementById('myTable').rows[i].cells[4].style.backgroundColor = 'lightgray';
		}
	}

}
function theyPassed()
{
	for(var i = 1; i < document.getElementById('myTable').getElementsByTagName('tr').length; i++)
	{
		if(document.getElementById('myTable').rows[i].cells[4].innerHTML >= 50)
		{
			document.getElementById('myTable').rows[i].cells[0].style.color = 'green';
			document.getElementById('myTable').rows[i].cells[1].style.color = 'green';
			document.getElementById('myTable').rows[i].cells[2].style.color = 'green';
			document.getElementById('myTable').rows[i].cells[3].style.color = 'green';
			document.getElementById('myTable').rows[i].cells[4].style.color = 'green';
		}
	}
}
function theyFailed()
{
	for(var i = 1; i < document.getElementById('myTable').getElementsByTagName('tr').length; i++)
	{
		if(document.getElementById('myTable').rows[i].cells[4].innerHTML < 50)
		{
			document.getElementById('myTable').rows[i].cells[0].style.color = 'red';
			document.getElementById('myTable').rows[i].cells[1].style.color = 'red';
			document.getElementById('myTable').rows[i].cells[2].style.color = 'red';
			document.getElementById('myTable').rows[i].cells[3].style.color = 'red';
			document.getElementById('myTable').rows[i].cells[4].style.color = 'red';
		}
	}
}
function removeFailed()
{
	for(var i = 1; i < document.getElementById('myTable').getElementsByTagName('tr').length; i++)
	{
		if(document.getElementById('myTable').rows[i].cells[4].innerHTML < 50)
		{
			document.getElementById('myTable').deleteRow(i);
			isSortDesc = false;
			sortTable()
		}
	}
}
