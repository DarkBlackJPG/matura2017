﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EIT_A9.localhost;

namespace EIT_A9
{
    public partial class Konvertor : System.Web.UI.Page
    {     
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Service1 latinicaucirilicu = new Service1();
            string rezultat = latinicaucirilicu.latinicaucirilicu(TextBox2.Text);
            Label4.Text = rezultat;
            Service1 cirilicaulatinicu = new Service1();
            string rezultat1 = cirilicaulatinicu.cirilicaulatinicu(TextBox1.Text);
            Label3.Text = rezultat1;
        }
    }
}