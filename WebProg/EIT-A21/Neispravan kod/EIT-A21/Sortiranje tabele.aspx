﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sortiranje tabele.aspx.cs" Inherits="EIT_A21.Sortiranje_tabele" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="CSSkod.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    
    <div>
    
        <asp:Table ID="MojaTabela" runat="server">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell ID="glava"><a href="#">Rbr</a></asp:TableHeaderCell>
                <asp:TableHeaderCell ID="mojaglava"><a href="#">Ime</a></asp:TableHeaderCell>
                <asp:TableHeaderCell ID="tvojaglava"><a href="#">Prezime</a></asp:TableHeaderCell>
            </asp:TableHeaderRow>
            <asp:TableRow>
                <asp:TableCell ID="z">1.</asp:TableCell>
                <asp:TableCell ID="mz">Pera</asp:TableCell>
                <asp:TableCell ID="tz">Mikic</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ID="z1">2.</asp:TableCell>
                <asp:TableCell ID="mz1">Mika</asp:TableCell>
                <asp:TableCell ID="tz1">Zikic</asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ID="z2">3.</asp:TableCell>
                <asp:TableCell ID="mz2">Zika</asp:TableCell>
                <asp:TableCell ID="tz2">Peric</asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/O aplikaciji.aspx">O aplikaciji</asp:HyperLink>
        <br />
    
    </div>
   
    <script src="KodZaFukncionalnost.js"></script>
</body>
</html>
