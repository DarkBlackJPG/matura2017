﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;


namespace EIT_A7
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        class Linija
        {
            string nazivLinije;
            public string NazivLinije
            {
                get { return nazivLinije; }
                set { nazivLinije = value; }
            }
            string[] rawArray;
            public string[] RawArray
            {
                get { return rawArray; }
                set { rawArray = value; }
            }
            //[row, column]
            public List<string> getValues(string smer)
            {
                
                List<string> values = new List<string>();
                int lastIndex = 0;
                int myIndex = 0;
                for ( int i = 0; i < rawArray.Length; i++)
                {
                    
                    if(rawArray[i].Contains(smer))
                    {
                        for(int j = i+1; j < rawArray.Length; j++)
                        {
                            if (rawArray[j].Contains("Smer") == true || rawArray[j] == "")
                            {
                                lastIndex = j;
                                myIndex = i;
                                break; 
                            }   
                        }
                    }

                    
                }
                for (int j = myIndex + 1; j < lastIndex; j++)
                {
                    values.Add(rawArray[j]);
                }
                //break;
                return values;
            }
            public List<string> getSmer()
            {
                List<string> smerovi = new List<string>();
                foreach(string x in rawArray)
                {
                    if (x.Contains("Smer"))
                    {
                        string[] newArray = x.Split(':');
                        smerovi.Add(newArray[1]);
                    }
                }
                return smerovi;
            }
            

        }
        List<Linija> myLinijaArray = new List<Linija>();
        protected void Button1_Click(object sender, EventArgs e)
        {
            if(DropDownList1.SelectedIndex == 0)
            {
                DropDownList2.Items.Clear();
                StreamReader srdr = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\EIT-A7\EIT-A7\Linije\Linija1.txt");
                string file = srdr.ReadToEnd();
                Linija Linija1 = new Linija();
                Linija1.RawArray = file.Split('\n');
                List<string> smerovi = Linija1.getSmer();
                Linija1.NazivLinije = DropDownList1.Text;
                List<string> items = new List<string>();
                foreach (string x in smerovi)
                {
                    items.Add(x);
                }

                DropDownList2.DataSource = items;
                DropDownList2.DataBind();
                

                myLinijaArray.Add(Linija1);
                srdr.Close();
            }
            else if(DropDownList1.SelectedIndex == 1)
            {
                DropDownList2.Items.Clear();
                StreamReader srdr2 = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\EIT-A7\EIT-A7\Linije\Linija2.txt");
                string file2 = srdr2.ReadToEnd();
                Linija Linija2 = new Linija();
                Linija2.RawArray = file2.Split('\n');
                List<string> smerovi2 = Linija2.getSmer();
                Linija2.NazivLinije = DropDownList1.Text;
                List<string> items = new List<string>();
                foreach (string x in smerovi2)
                {
                    items.Add(x);
                }

                DropDownList2.DataSource = items;
                DropDownList2.DataBind();
                myLinijaArray.Add(Linija2);
                srdr2.Close();
            }  
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        
        protected void Button2_Click(object sender, EventArgs e)
        {
            
            List<string> getValues = new List<string>();
            if (DropDownList1.SelectedIndex == 0)
            {
                StreamReader srdr = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\EIT-A7\EIT-A7\Linije\Linija1.txt");
                string file = srdr.ReadToEnd();
                Linija Linija1 = new Linija();
                Linija1.RawArray = file.Split('\n');
                List<string> values = Linija1.getValues(DropDownList2.SelectedItem.Text);
                
                for (int i = 0; i < values.Count; i++)
                {
                    TableRow tr = new TableRow();
                    TableCell tc2 = new TableCell();
                    tc2.Text = values[i];
                    TableCell tc = new TableCell();
                    tc.Text = (i+1).ToString();
                    tr.Cells.Add(tc);
                    tr.Cells.Add(tc2);
                    Table1.Rows.Add(tr);
                 }
                srdr.Close();
            }
            else if (DropDownList1.SelectedIndex == 1)
            {
                StreamReader srdr = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\EIT-A7\EIT-A7\Linije\Linija2.txt");
                string file = srdr.ReadToEnd();
                Linija Linija1 = new Linija();
                Linija1.RawArray = file.Split('\n');
                List<string> values = Linija1.getValues(DropDownList2.Text);

                for (int i = 0; i < values.Count; i++)
                {
                    TableRow tr = new TableRow();
                    TableCell tc2 = new TableCell();
                    tc2.Text = values[i];
                    TableCell tc = new TableCell();
                    tc.Text = (i + 1).ToString();
                    tr.Cells.Add(tc);
                    tr.Cells.Add(tc2);
                    Table1.Rows.Add(tr);
                }
                srdr.Close();
            }
        }

        //protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DropDownList2.DataBind();
        //}
    }
}