﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_SHARED.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="RedVoznje.aspx.cs" Inherits="EIT_A7.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Text="Autobuska linija"></asp:Label>  
&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="DropDownList1" runat="server">
        <asp:ListItem Value="Linija1">Linija 1</asp:ListItem>
        <asp:ListItem Value="Linija2">Linija 2</asp:ListItem>
    </asp:DropDownList>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Smer"></asp:Label>
&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="True" CausesValidation="True">
    </asp:DropDownList>
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Filtriraj Smerove" />
    <br />
    <asp:Button ID="Button2" runat="server"  OnClick="Button2_Click" Text="Ucitaj" />
    <br />
    <asp:Table ID="Table1" runat="server" CellPadding="5" GridLines="Vertical">
        <asp:TableHeaderRow>
            <asp:TableCell>Redni broj polaska</asp:TableCell>
            <asp:TableCell>Vreme Polaska</asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
    <br />
</asp:Content>
