﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;

namespace EIT_A10Service
{
    /// <summary>
    /// Summary description for prviServis
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]

    public class kursValuta
    {
        DateTime datum;
        string valuta;
        double kurs;
        public void upisiUMene(string element)
        {
            if (element != "") {
                string[] x = element.Split('|');
                datum = Convert.ToDateTime(x[0]);
                valuta = x[1];
                kurs = Convert.ToDouble(x[2]);
            }
        }
        public DateTime Datum
        {
            get { return datum; }
        }
        public string Valuta
        {
            get { return valuta; }
        }
        public double Kurs
        {
            get { return kurs; }
        }
    }
    public class prviServis : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            
            return "Hello World";
        }

        [WebMethod]

        public double doubleProcitajKursNaDan(DateTime Datum, string valuta)
        {
            List<kursValuta> array = new List<kursValuta>();
            StreamReader srdr = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\WebProg\EIT-A10Service\EIT-A10Service\kursnaLista.txt");
            string[] elements = srdr.ReadToEnd().Split('\n');
            foreach(string x in elements)
            {
                kursValuta elementX = new kursValuta();
                elementX.upisiUMene(x);
                array.Add(elementX);
            }
            kursValuta myElement = new kursValuta();
            foreach(kursValuta x in array)
            {
                if(Datum.Equals(x.Datum))
                {
                    if (valuta == x.Valuta.Trim(' '))
                    {
                        myElement = x;
                    }
                }
            }
            srdr.Close();
            return myElement.Kurs;
           
        }

        [WebMethod]
        
        public bool boolUpisiKursNaDan(DateTime Datum, string valuta, double Kurs)
        {
            bool x = true;
            StreamWriter srwr = new StreamWriter(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\WebProg\EIT-A10Service\EIT-A10Service\kursnaLista.txt", true);
            try {
                
                srwr.WriteLine(Datum.Year+"-"+Datum.Month+"-"+Datum.Day + " | " + valuta + " | " + Kurs.ToString());
                
            }
            catch(Exception ex)
            {
                 x = false;
            }
            finally
            {
                srwr.Close();
                x = true;
            }
            return x;
        }

        [WebMethod]

        public List<string> ProcitajSveValute()
        {
            List<string> array = new List<string>();
            StreamReader srdr = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\WebProg\EIT-A10Service\EIT-A10Service\kursnaLista.txt");
            string[] elements = srdr.ReadToEnd().Split('\n');
            foreach (string x in elements)
            {
                kursValuta elementX = new kursValuta();
                elementX.upisiUMene(x);
                array.Add(elementX.Valuta);
            }
            srdr.Close();

            return array;
        }
        
    }
}
