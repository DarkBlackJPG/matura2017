﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace EIT_A8
{
    public partial class Web_prodavnica : System.Web.UI.Page
    {
      
        class Item
        {
            string id, name, manufacturer, ram, processor, camera, display, picture, price;

            public string ID
            {
                get { return id; }
                set { id = value; }
            }
            public string Name
            {
                get { return name; }
                set { name = value; }
            }
            public string Manufacture
            {
                get { return manufacturer; }
                set { manufacturer = value; }
            }
            public string Ram
            {
                get { return ram; }
                set { ram = value; }
            }
            public string Procesor
            {
                get { return processor; }
                set { processor = value; }
            }
            public string Kamera
            {
                get { return camera; }
                set { camera = value; }
            }
            public string Display
            {
                get { return display; }
                set { display = value; }
            }
            public string Picture
            {
                get { return picture; }
                set { picture = value; }
            }
            public string Price
            {
                get { return price; }
                set { price = value; }
            }

            public void insertValues(string item)
            {
                char[] charArray = item.ToCharArray();
                string newID = "";
                for (int i = 0; i <= 5; i++ )
                {
                    newID += charArray[i].ToString();
                    id = newID.Trim(' ');
                }
                string newName = "";
                for (int i = 6; i <= 30; i++)
                {
                   
                    newName += charArray[i].ToString();
                    name = newName.Trim(' ');
                }
                string newManufacture = "";
                for (int i = 31; i <= 50; i++)
                {
                   
                    newManufacture += charArray[i].ToString();
                    manufacturer = newManufacture.Trim(' ');
                }
                string newRam = "";
                for (int i = 51; i <= 55; i++)
                {
                   
                    newRam += charArray[i].ToString();
                    ram = newRam.Trim(' ');
                }
                string newProcesor = "";
                for (int i = 56; i <= 70; i++)
                {
                   
                    newProcesor += charArray[i].ToString();
                    processor = newProcesor.Trim(' ');
                }
                string newKamera = "";
                for (int i = 71; i <= 80; i++)
                {
                    
                    newKamera += charArray[i].ToString();
                    camera = newKamera.Trim(' ');
                }
                string newEkran = "";
                for (int i = 81; i <= 85; i++)
                {
                   
                    newEkran += charArray[i].ToString();
                    display = newEkran.Trim(' ');
                }
                string newSlika = "";
                for (int i = 91; i <= 120; i++)
                {
                   
                    newSlika += charArray[i].ToString();
                    picture = newSlika.Trim(' '); 
                }
                string newCena = "";
                for (int i = 121; i <= 130; i++)
                {
                   
                    newCena += charArray[i].ToString();
                    price = newCena.Trim(' ');
                }

            }

        }

        List<Item> itemList = new List<Item>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (itemList.Count <= 0)
            {
                StreamReader srdr = new StreamReader(@"C:\Users\Stefan Teslic\Desktop\MaturaGit\matura2017\WebProg\EIT-A8\EIT-A8\vebprodavnica.txt");
                /** 
                * string elementsString = srdr.ReadToEnd();
                * string[] elementsArray = elementsString.Split('\n');
                **/
                string[] itemArray = srdr.ReadToEnd().Split('\n');
                foreach (string x in itemArray)
                {
                    Item myItem = new Item();
                    myItem.insertValues(x);
                    itemList.Add(myItem);
                }
                srdr.Close();
            }
            if(!IsPostBack)
            {
                foreach(Item x in itemList)
                {
                    DropDownList1.Items.Add(x.Manufacture.Trim());
                    DropDownList3.Items.Add(x.Procesor.Trim());
                    DropDownList4.Items.Add(x.Kamera.Trim());
                    DropDownList5.Items.Add(x.Display.Trim());
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            foreach(Item x in itemList)
            {
                if(x.Manufacture.Contains(DropDownList1.SelectedItem.Text) && x.Ram.Contains(DropDownList2.SelectedItem.Text) && x.Procesor.Contains(DropDownList3.SelectedItem.Text) && x.Kamera.Contains(DropDownList4.SelectedItem.Text) && x.Display.Contains(DropDownList5.SelectedItem.Text))
                {
                    TableRow tr = new TableRow();
                    TableCell tc1 = new TableCell();
                    TableCell tc2 = new TableCell();
                    TableCell tc3 = new TableCell();
                    TableCell tc4 = new TableCell();
                    TableCell tc5 = new TableCell();
                    TableCell tc6 = new TableCell();
                    TableCell tc7 = new TableCell();
                    TableCell tc8 = new TableCell();
                    TableCell tc9 = new TableCell();

                    tc1.Text = x.ID;
                    tc2.Text = x.Name;
                    tc3.Text = x.Manufacture;
                    tc4.Text = x.Ram;
                    tc5.Text = x.Procesor;
                    tc6.Text = x.Kamera;
                    tc7.Text = x.Display;
                    tc8.Text = string.Format("<img width='100px' height='50px' src='{0}' />", x.Picture.Trim('"'));
                    tc9.Text = x.Price;

                    tr.Cells.Add(tc1);
                    tr.Cells.Add(tc2);
                    tr.Cells.Add(tc3);
                    tr.Cells.Add(tc4);
                    tr.Cells.Add(tc5);
                    tr.Cells.Add(tc6);
                    tr.Cells.Add(tc7);
                    tr.Cells.Add(tc8);
                    tr.Cells.Add(tc9);

                    Table1.Rows.Add(tr);

                }
            }
        }
    }
}