﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Web prodavnica.aspx.cs" Inherits="EIT_A8.Web_prodavnica" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="CSSkod.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Proizvodjac"></asp:Label>
        &nbsp; <asp:DropDownList ID="DropDownList1" runat="server">
        </asp:DropDownList>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Ram memorija"></asp:Label>
        &nbsp;<asp:DropDownList ID="DropDownList2" runat="server">
            <asp:ListItem>512MB</asp:ListItem>
            <asp:ListItem>1GB</asp:ListItem>
            <asp:ListItem>1.5GB</asp:ListItem>
            <asp:ListItem>2GB</asp:ListItem>
            <asp:ListItem>3GB</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Procesor"></asp:Label>
        &nbsp;<asp:DropDownList ID="DropDownList3" runat="server">
        </asp:DropDownList>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="Kamera"></asp:Label>
        &nbsp;<asp:DropDownList ID="DropDownList4" runat="server">
        </asp:DropDownList>
        <br />
        <br />
        <asp:Label ID="Label5" runat="server" Text="Ekran"></asp:Label>
    
        &nbsp;<asp:DropDownList ID="DropDownList5" runat="server">
        </asp:DropDownList>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Trazi" OnClick="Button1_Click" Width="124px" />
&nbsp;<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Korisnicko uputstvo.aspx">Uputstvo</asp:HyperLink>
    
        <br />
        <br />
        <asp:Table ID="Table1" runat="server" BorderStyle="Solid" BorderWidth="1px" CellPadding="10">
            <asp:TableHeaderRow BorderStyle="Outset">
                <asp:TableCell>Sifra</asp:TableCell>
                <asp:TableCell>Naziv</asp:TableCell>
                <asp:TableCell>Proizvodjac</asp:TableCell>
                <asp:TableCell>RAM memorija</asp:TableCell>
                <asp:TableCell>Tip Procesora</asp:TableCell>
                <asp:TableCell>Kamera</asp:TableCell>
                <asp:TableCell>Ekran</asp:TableCell>
                <asp:TableCell>Slika</asp:TableCell>
                <asp:TableCell>Cena</asp:TableCell>
            </asp:TableHeaderRow>
        </asp:Table>
    
        <br />
        <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
    
    </div>
    </form>
</body>
</html>
