﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maturaEIT_B16
{
    public partial class Citaoci : Form
    {
        SqlConnection conn = new SqlConnection(@"Data Source=FX6300-PC;Initial Catalog=maturaEIT-B16;Integrated Security=True");
        public Citaoci()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void Citaoci_Load(object sender, EventArgs e)
        {
            try
            {
                SqlCommand selectsve = new SqlCommand("SELECT * FROM Citalac", conn);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(selectsve);
                DataSet ds = new DataSet();
                da.Fill(ds, "Citalac");
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Citalac";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

            finally
            {
                conn.Close();
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            string status = "Uspesno upisano!";
            try
            {
                SqlCommand insert = new SqlCommand("INSERT into Citalac(MaticniBroj,Ime,Prezime,Mesto,Adresa,Telefon) VALUES('" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + textBox5.Text + "','" + textBox6.Text + "','" + textBox7.Text + "')", conn);
                conn.Open();
                insert.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                status = "Neuspesno upisano!";

            }

            finally
            {
                MessageBox.Show(status);
                conn.Close();
                SqlCommand selectsve = new SqlCommand("SELECT * FROM Citalac", conn);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(selectsve);
                DataSet ds = new DataSet();
                da.Fill(ds, "Citalac");
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Citalac";
                conn.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string status = "Uspesno obrisano!";
            try
            {
                SqlCommand delete = new SqlCommand("DELETE from Citalac where Citalac.CitalacID=" + textBox1.Text, conn);
                conn.Open();
                delete.ExecuteNonQuery();

                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                status = "Neuspesno obrisano!";

            }

            finally
            {
                MessageBox.Show(status);
                conn.Close();
                SqlCommand selectsve = new SqlCommand("SELECT * FROM Citalac", conn);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(selectsve);
                DataSet ds = new DataSet();
                da.Fill(ds, "Citalac");
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Citalac";
                conn.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string status = "Uspesno izmenjeno!";
            try
            {

                SqlCommand update = new SqlCommand("UPDATE Citalac SET  MaticniBroj='" + textBox2.Text + "', Ime='" + textBox3.Text + "', Prezime='" + textBox4.Text + "', Mesto='" + textBox5.Text + "', Adresa='" + textBox6.Text + "', Telefon='" + textBox7.Text + "' WHERE CitalacID=" + textBox1.Text, conn);
                conn.Open();
                update.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                status = "Neuspesno izmenjeno!";

            }

            finally
            {
                MessageBox.Show(status);
                conn.Close();
                SqlCommand selectsve = new SqlCommand("SELECT * FROM Citalac", conn);
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(selectsve);
                DataSet ds = new DataSet();
                da.Fill(ds, "Citalac");
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Citalac";
                conn.Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != "")
                {
                    SqlCommand select = new SqlCommand("SELECT * from Citalac WHERE Citalac.CitalacID=" + textBox1.Text, conn);
                    conn.Open();
                    SqlDataReader rd = select.ExecuteReader();
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox6.Text = "";
                    textBox7.Text = "";

                    while (rd.Read())
                    {
                        textBox1.Text = rd[0].ToString();
                        textBox2.Text = rd[1].ToString();
                        textBox3.Text = rd[2].ToString();
                        textBox4.Text = rd[3].ToString();
                        textBox5.Text = rd[4].ToString();
                        textBox6.Text = rd[5].ToString();
                        textBox7.Text = rd[6].ToString();

                    }
                }
                else
                {
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox6.Text = "";
                    textBox7.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            finally
            {
                conn.Close();
            }
        }
    }
}
