﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace maturaEIT_B16
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void citaociToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Citaoci formica = new Citaoci();
            formica.ShowDialog();
        }

        private void poCitaocimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Po_citaocima formica = new Po_citaocima();
            formica.ShowDialog();
        }
    }
}
