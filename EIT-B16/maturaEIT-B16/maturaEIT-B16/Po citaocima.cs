﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace maturaEIT_B16
{
    public partial class Po_citaocima : Form
    {
        SqlConnection conn = new SqlConnection(@"Data Source=FX6300-PC;Initial Catalog=maturaEIT-B16;Integrated Security=True");
        public Po_citaocima()
        {
            InitializeComponent();
        }

        private void chart1_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int jan=0,feb=0,mar=0,apr=0,maj=0,jun=0,jul=0,avg=0,sep=0,okt=0,nov=0,dec =0 ;
            string[] rastavljen = comboBox1.SelectedItem.ToString().Split('-');
            int id = Convert.ToInt32(rastavljen[0]);
            SqlCommand select = new SqlCommand("SELECT DatumUzimanja from Na_Citanju, Knjiga, Citalac WHERE Na_Citanju.DatumUzimanja like '"+textBox1.Text+"%' and Na_Citanju.CitalacID="+id+" and Na_Citanju.KnjigaID=Knjiga.KnjigaID and Na_Citanju.CitalacID=Citalac.CitalacID ", conn);
            conn.Open();
            SqlDataReader rd = select.ExecuteReader();
            DateTime januar = new DateTime(2000,1,1);
            DateTime februar = new DateTime(2000, 2, 1);
            DateTime mart = new DateTime(2000, 3, 1);
            DateTime april = new DateTime(2000, 4, 1);
            DateTime maja = new DateTime(2000, 5, 1);
            DateTime juna = new DateTime(2000, 6, 1);
            DateTime jula = new DateTime(2000, 7, 1);
            DateTime avgust = new DateTime(2000, 8, 1);
            DateTime septembar = new DateTime(2000, 9, 1);
            DateTime oktobar = new DateTime(2000, 10, 1);
            DateTime novembar = new DateTime(2000, 11, 1);
            DateTime decembar = new DateTime(2000, 12, 1);
            while (rd.Read())
            {
                //string[] datum = rd[0].ToString().Split('-');
                DateTime nesto = Convert.ToDateTime(rd[0]);
                if (nesto.Month == januar.Month)
                    jan++;
                else if (nesto.Month == februar.Month)
                    feb++;
                else if (nesto.Month == mart.Month)
                    mar++;
                else if (nesto.Month == april.Month)
                    apr++;
                else if (nesto.Month == maja.Month)
                    maj++;
                else if (nesto.Month == juna.Month)
                    jun++;
                else if (nesto.Month == jula.Month)
                    jul++;
                else if (nesto.Month== avgust.Month)
                    avg++;
                else if (nesto.Month == septembar.Month)
                    sep++;
                else if (nesto.Month == oktobar.Month)
                    okt++;
                else if (nesto.Month == novembar.Month)
                    nov++;
                else if (nesto.Month == decembar.Month)
                    dec++;
            }
            chart1.Series["Broj knjiga"].Points.AddXY("Januar", jan);
            chart1.Series["Broj knjiga"].Points.AddXY("Februar", feb);
            chart1.Series["Broj knjiga"].Points.AddXY("Mart", mar);
            chart1.Series["Broj knjiga"].Points.AddXY("April", apr);
            chart1.Series["Broj knjiga"].Points.AddXY("Maj", maj);
            chart1.Series["Broj knjiga"].Points.AddXY("Jun", jun);
            chart1.Series["Broj knjiga"].Points.AddXY("Jul", jul);
            chart1.Series["Broj knjiga"].Points.AddXY("Avgust", avg);
            chart1.Series["Broj knjiga"].Points.AddXY("Septembar", sep);
            chart1.Series["Broj knjiga"].Points.AddXY("Oktobar", okt);
            chart1.Series["Broj knjiga"].Points.AddXY("Novembar", nov);
            chart1.Series["Broj knjiga"].Points.AddXY("Decembar", dec);
            
            conn.Close();
        }

        private void Po_citaocima_Load(object sender, EventArgs e)
        {
            string sifra = "";
            string ime = "";
            string prezime = "";
            SqlCommand select = new SqlCommand("SELECT CitalacID, Ime, Prezime from Citalac", conn);
            conn.Open();
            SqlDataReader rd = select.ExecuteReader();
            while (rd.Read())
            {
                sifra = rd[0].ToString();
                ime = rd[1].ToString();
                prezime = rd[2].ToString();
                string sve = sifra + "-" + ime + prezime;
                comboBox1.Items.Add(sve);
            }
            conn.Close();
        }
    }
}
