﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EIT_B13
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void izlazToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void knjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            unos_knjiga form = new unos_knjiga();
            form.ShowDialog();
        }

        private void autoriToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void poKategorijamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            po_kategorijama form = new po_kategorijama();
            form.ShowDialog();
        }

        private void poAutorimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
    }
}
