﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace EIT_B13
{
    public partial class unos_knjiga : Form
    {
        public unos_knjiga()
        {
            InitializeComponent();
        }

        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EITB13;Integrated Security=True");

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            button2.Enabled = false;
            button1.Enabled = true;
            comboBox1.Enabled = true;
            textBox2.Enabled = true;
            comboBox2.Enabled = true;
            textBox3.Enabled = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
            button1.Enabled = false;
            comboBox1.Enabled = false;
            textBox2.Enabled = false;
            comboBox2.Enabled = false;
            textBox3.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "INSERT INTO Knjiga(knjigaID, naziv, brojStrana, kategorijaID, komentar) values (@id, @naziv, @broj, @kat, @kom)";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                //Deklarisem id da bi funkcija findCategory mogla da vrati vrednost ID koju pronadje za datu kategoriju
                string id = "";
                findCategory(comboBox2.Text, out id); //Funkcija definisana dole, sluzi da pronadje ID naziva kategorije
                cmd.Parameters.Add("@naziv", SqlDbType.Text).Value = comboBox1.Text;
                cmd.Parameters.Add("@broj", SqlDbType.Int).Value = textBox2.Text;
                cmd.Parameters.Add("@kat", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@kom", SqlDbType.Text).Value = textBox3.Text;

                bool result = false;
                checkIfUpdate(id,out result);
                string upis = "Uspesno upisano!";
                if(result)
                {
                    cmd.CommandText = "Update Knjiga set naziv = @naziv, brojStrana = @broj, kategorijaID = @kat, komentar = @kom where knjigaID = @id";
                    upis = "Uspesno azurirano";
                }

                _CONN.Open();
                if(cmd.ExecuteNonQuery() >=1 )
                {
                    MessageBox.Show(upis);
                }
                else
                {
                    throw new Exception("Nista nije upisano!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
                refreshData();
            }
            
        }

        public void refreshData()
        {
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "SELECT naziv from Kategorija";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "SELECT naziv from Knjiga";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
        public void findCategory(string name, out string id)
        {
            List<string> list = new List<string>();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select kategorijaID from Kategorija where naziv = @naziv";
                cmd.Parameters.Add("@naziv", SqlDbType.VarChar).Value = name;
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    list.Add(rdr[0].ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
            id = list[0];
        }

        private void unos_knjiga_Load(object sender, EventArgs e)
        {
            refreshData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void checkIfUpdate(string id, out bool result)
        {
            bool x = false;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "select * from Knjiga where Knjiga.knjigaID = @id";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                if(rdr.HasRows)
                {
                    x = true;
                }
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
              
            }
            result = x;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "delete from Knjiga where Knjiga.knjigaID = @id";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                _CONN.Open();
                if (cmd.ExecuteNonQuery() >= 1)
                {
                    MessageBox.Show("Uspesno obrisano!");
                }
                else throw new Exception("Nista nije obrisano!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
                refreshData();
            }
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            comboBox1.Text = "";
            comboBox2.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            comboBox1.Text = "";
            textBox2.Text = "";
            comboBox2.Text = "";
            textBox3.Text = "";
            if (textBox1.Text != "")
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "select Knjiga.naziv, Knjiga.brojStrana, Kategorija.naziv as kategorija, Knjiga.komentar from Knjiga, Kategorija where Knjiga.knjigaID = @id and Knjiga.kategorijaID = Kategorija.kategorijaID;";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            comboBox1.Text = rdr[0].ToString();
                            textBox2.Text = rdr[1].ToString();
                            comboBox2.Text = rdr[2].ToString();
                            textBox3.Text = rdr[3].ToString();
                        }
                    }
                    else
                    {
                        if (radioButton2.Checked)
                        {
                            throw new Exception("Nema pronadjenih elemenata za brisanje!");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
        }
    }
}
