﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace EIT_B13
{
    public partial class po_kategorijama : Form
    {
        public po_kategorijama()
        {
            InitializeComponent();
        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EITB13;Integrated Security=True");
        private void po_kategorijama_Load(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "SELECT naziv from Kategorija";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    checkedListBox1.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

       
        public void findCategory(string name, out string id)
        {
            List<string> list = new List<string>();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select kategorijaID from Kategorija where naziv = @naziv";
                cmd.Parameters.Add("@naziv", SqlDbType.VarChar).Value = name;
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    list.Add(rdr[0].ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
            id = list[0];
        }
        private void button1_Click(object sender, EventArgs e)
        {
            chart1.Series[0].Points.Clear();
            List<string> checkedList = new List<string>();
            foreach(var x in checkedListBox1.CheckedItems)
            {
                checkedList.Add(x.ToString());
            }
           
            if (checkedListBox1.CheckedItems.Count >= 1)
            {
                foreach (string x in checkedList) {
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = _CONN;
                        cmd.CommandText = "select distinct Kategorija.naziv, Count(Knjiga.kategorijaID) from Knjiga, Kategorija where Knjiga.kategorijaID = @id and Knjiga.kategorijaID = Kategorija.kategorijaID group by Knjiga.kategorijaID, Kategorija.naziv";
                        string id = "";
                        findCategory(x, out id);
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        _CONN.Open();
                        SqlDataReader rdr = cmd.ExecuteReader();
                        
                        while (rdr.Read())
                        {
                           chart1.Series[0].Points.AddXY(rdr[0].ToString(), Convert.ToInt32(rdr[1].ToString()));
                        }
                        
                       
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        _CONN.Close();
                       
                    }
 }
            }
            else MessageBox.Show("Nista nije selektovano!");

          
        }
    }
}
