﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EIT_B2
{
    public partial class Unos : Form
    {
        public Unos()
        {
            InitializeComponent();
        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-B2;Integrated Security=True");
        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void changeState()
        {
            if(radioButton1.Checked)
            {
                textBox1.Enabled = false;
                textBox2.Enabled = true;
                dateTimePicker1.Enabled = true;
                button2.Enabled = false;
                button1.Enabled = true;
            }
            else if(radioButton2.Checked)
            {
                textBox1.Enabled = true;
                textBox2.Enabled = false;
                dateTimePicker1.Enabled = false;
                button1.Enabled = false;
                button2.Enabled = true;
            }
        }
        private void Unos_Load(object sender, EventArgs e)
        {
            changeState();
        }
        public string makeID()
        {
            string ID = "";
            if(textBox2.Text.Length >= 3)
            {
                DateTime date = dateTimePicker1.Value;
                char[] city = textBox2.Text.ToCharArray();
                if (date.Month <= 9)
                    ID = city[0].ToString().ToUpper() + city[1].ToString().ToUpper() + city[2].ToString().ToUpper() + "0" + date.Month + date.Year;
                else if(date.Month >= 10)
                    ID = (city[0] + city[1] + city[2]).ToString().ToUpper() + date.Month + date.Year;

            }
            return ID;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            changeState();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            changeState();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "INSERT INTO Izlozba VALUES(@id, @mesto, @datum)";
                cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = textBox1.Text;
                cmd.Parameters.Add("@mesto", SqlDbType.VarChar).Value = textBox2.Text;
                cmd.Parameters.Add("@datum", SqlDbType.Date).Value = dateTimePicker1.Value;
                _CONN.Open();
                if (cmd.ExecuteNonQuery() >= 1)
                {
                    MessageBox.Show("Uspesno dodato!");
                }
                else
                    throw new Exception("Doslo je do nekih problema");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
                textBox1.Text = makeID();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            textBox1.Text = makeID();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "DELETE FROM Izlozba WHERE IzlozbaID ='" + textBox1.Text+"'";
                _CONN.Open();
                if(MessageBox.Show(null,"Da li ste sigurni?","Upozorenje!",MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (cmd.ExecuteNonQuery() >= 1)
                    {
                        MessageBox.Show("Uspesno Obrisano!");
                    }
                    else
                        throw new Exception("Doslo je do problema");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "SELECT Mesto, Datum from Izlozba where IzlozbaID ='" + textBox1.Text+"'";
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            textBox2.Text = rdr[0].ToString();
                            dateTimePicker1.Value = Convert.ToDateTime(rdr[1].ToString());
                        }
                    }
                    else
                    {
                        textBox2.Text = "";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
        }
    }
}
