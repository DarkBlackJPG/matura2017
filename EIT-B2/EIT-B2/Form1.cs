﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EIT_B2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void unosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Unos form = new Unos();
            form.ShowDialog();
        }

        private void spisakPasaPoRasiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Spisak_Pasa_Po_Rasi form = new Spisak_Pasa_Po_Rasi();
            form.ShowDialog();
        }
    }
}
