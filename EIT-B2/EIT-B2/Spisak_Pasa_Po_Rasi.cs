﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EIT_B2
{
    public partial class Spisak_Pasa_Po_Rasi : Form
    {
        public Spisak_Pasa_Po_Rasi()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-B2;Integrated Security=True");
        private void Spisak_Pasa_Po_Rasi_Load(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select IzlozbaID from Izlozba";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    comboBox1.Items.Add(rdr[0].ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                _CONN.Close();
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select NazivRase from Rasa";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                _CONN.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "SELECT distinct Pas.Ime as [Ime psa] FROM Izlozba, Rezultat, Pas, Rasa WHERE Rezultat.IzlozbaID = @izlozba AND Rezultat.PasID = Pas.PasID AND Pas.RasaID = (SELECT Rasa.RasaID from Rasa where Rasa.NazivRase = @rasa) ";
                cmd.Parameters.Add("@izlozba", SqlDbType.VarChar).Value = comboBox1.Text;
                cmd.Parameters.Add("@rasa", SqlDbType.VarChar).Value = comboBox2.Text;
                _CONN.Open();
                SqlDataAdapter adr = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adr.Fill(dt);
                dataGridView1.DataSource = dt;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
            label6.Text = (dataGridView1.Rows.Count-1).ToString();

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select COUNT(IzlozbaID) from Rezultat group by IzlozbaID";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                if(rdr.HasRows)
                {
                    while(rdr.Read())
                         label5.Text = rdr[0].ToString();
                }
                else
                {
                    label5.Text = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
    }
}
