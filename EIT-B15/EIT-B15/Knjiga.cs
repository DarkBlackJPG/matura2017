﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EIT_B15
{
    public partial class Knjiga : Form
    {
        public Knjiga()
        {
            InitializeComponent();
        }

        private void Knjiga_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'database1DataSet.Knjiga' table. You can move, or remove it, as needed.
            this.knjigaTableAdapter.Fill(this.database1DataSet.Knjiga);
            button2.Enabled = false;
            button3.Enabled = false;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\Skola\EIT-B15\EIT-B15\Database1.mdf;Integrated Security=True");
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("SELECT * FROM Knjiga WHERE KnjigaID='" + textBox1.Text + "';", conn);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    button1.Enabled = false;
                    button2.Enabled = true;
                    button3.Enabled = true;
                    textBox2.Text = reader[1].ToString();
                    textBox3.Text = reader[2].ToString();
                    textBox4.Text = reader[3].ToString();
                }
                else 
                {
                    textBox2.Clear();
                    textBox3.Clear();
                    textBox4.Clear();
                    button1.Enabled = true;
                    button2.Enabled = false;
                    button3.Enabled = false;
                }
                reader.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally { conn.Close(); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\Skola\EIT-B15\EIT-B15\Database1.mdf;Integrated Security=True");
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("DELETE FROM Knjiga WHERE KnjigaID='" + textBox1.Text + "';", conn);
                command.ExecuteNonQuery();
                textBox1.Clear();
                MessageBox.Show("Uspesno ste obrisali knjigu!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally 
            { 
                conn.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\Skola\EIT-B15\EIT-B15\Database1.mdf;Integrated Security=True");
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("INSERT INTO Knjiga VALUES('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','1');", conn);
                command.ExecuteNonQuery();
                MessageBox.Show("Uspesno ste upisali novu knjigu!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\Skola\EIT-B15\EIT-B15\Database1.mdf;Integrated Security=True");
            try
            {
                conn.Open();
                SqlCommand command = new SqlCommand("UPDATE Knjiga SET  UDK='" + textBox2.Text + "', ISBN='" + textBox3.Text + "', Naziv='" + textBox4.Text + "' WHERE KnjigaID='"+textBox1.Text+"';", conn);
                command.ExecuteNonQuery();
                MessageBox.Show("Uspesno ste izmenili knjigu!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
