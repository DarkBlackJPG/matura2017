﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EIT_B15
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void knjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Knjiga forma = new Knjiga();
            forma.ShowDialog();
        }

        private void poKnjigamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Po_Knjigama forma = new Po_Knjigama();
            forma.ShowDialog();
        }
    }
}
