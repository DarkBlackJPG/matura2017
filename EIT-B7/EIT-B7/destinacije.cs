﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EIT_B7
{
    public partial class destinacije : Form
    {
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-4GG1978;Initial Catalog=turisticka_agencija;Integrated Security=True");
        List<int> destinationIDs = new List<int>();
        public destinacije()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void addIDs()
        {

            destinationIDs.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select turisticka_destinacija.destinacijaID from turisticka_destinacija order by turisticka_destinacija.destinacijaID asc";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    destinationIDs.Add(Convert.ToInt32(rdr[0]));
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }

        public void reset()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            reset();
            textBox1.Focus();
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
           if(textBox1.Text == "")
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = destinationIDs.Min();
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        textBox1.Text = rdr[0].ToString();
                        textBox2.Text = rdr[1].ToString();
                        textBox3.Text = rdr[2].ToString();
                        textBox4.Text = rdr[3].ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
           else
            {
                if(Convert.ToInt32(textBox1.Text) != destinationIDs.Min())
                {
                    int lowerID = destinationIDs[destinationIDs.IndexOf(Convert.ToInt32(textBox1.Text)) - 1];
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = _CONN;
                        cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = lowerID;
                        _CONN.Open();
                        SqlDataReader rdr = cmd.ExecuteReader();
                        while (rdr.Read())
                        {
                            textBox1.Text = rdr[0].ToString();
                            textBox2.Text = rdr[1].ToString();
                            textBox3.Text = rdr[2].ToString();
                            textBox4.Text = rdr[3].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        _CONN.Close();
                    }
                }
                else if (Convert.ToInt32(textBox1.Text) == destinationIDs.Min())
                {
                    MessageBox.Show("Ovo je prvi elemenat u tabeli!");
                }
            }
        }

        private void destinacije_Load(object sender, EventArgs e)
        {
            addIDs();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox1.Text != "")
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        textBox1.Text = rdr[0].ToString();
                        textBox2.Text = rdr[1].ToString();
                        textBox3.Text = rdr[2].ToString();
                        textBox4.Text = rdr[3].ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
            else
            {
                reset();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = destinationIDs.Min();
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        textBox1.Text = rdr[0].ToString();
                        textBox2.Text = rdr[1].ToString();
                        textBox3.Text = rdr[2].ToString();
                        textBox4.Text = rdr[3].ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
            else
            {
                if (Convert.ToInt32(textBox1.Text) != destinationIDs.Max())
                {
                    int higherID = destinationIDs[destinationIDs.IndexOf(Convert.ToInt32(textBox1.Text)) + 1];
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = _CONN;
                        cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = higherID;
                        _CONN.Open();
                        SqlDataReader rdr = cmd.ExecuteReader();
                        while (rdr.Read())
                        {
                            textBox1.Text = rdr[0].ToString();
                            textBox2.Text = rdr[1].ToString();
                            textBox3.Text = rdr[2].ToString();
                            textBox4.Text = rdr[3].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        _CONN.Close();
                    }
                }
                else if (Convert.ToInt32(textBox1.Text) == destinationIDs.Max())
                {
                    MessageBox.Show("Ovo je poslednji elemenat u tabeli!");
                }
            }
        }

      
        private void button4_Click(object sender, EventArgs e)
        {
            bool result = false;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }

            if(result)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "update turisticka_destinacija set nazivMesta = @mesto, drzava = @drzava, cenaVize = @cena where destinacijaID = @id";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                    cmd.Parameters.Add("@cena", SqlDbType.Int).Value = textBox4.Text;
                    cmd.Parameters.Add("@drzava", SqlDbType.Text).Value = textBox3.Text;
                    cmd.Parameters.Add("@mesto", SqlDbType.Text).Value = textBox2.Text;
                    _CONN.Open();
                    if(cmd.ExecuteNonQuery() >= 1)
                    {
                        MessageBox.Show("Uspesno azurirano!");
                    }
                    else
                    {
                        throw new Exception("Doslo je do greske, nista nije azurirano");
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        textBox1.Text = rdr[0].ToString();
                        textBox2.Text = rdr[1].ToString();
                        textBox3.Text = rdr[2].ToString();
                        textBox4.Text = rdr[3].ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
            else
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "insert into turisticka_destinacija(nazivMesta, drzava, cenaVize) values (@mesto, @drzava, @cena)";
                    cmd.Parameters.Add("@cena", SqlDbType.Int).Value = textBox4.Text;
                    cmd.Parameters.Add("@drzava", SqlDbType.Text).Value = textBox3.Text;
                    cmd.Parameters.Add("@mesto", SqlDbType.Text).Value = textBox2.Text;
                    _CONN.Open();
                    if (cmd.ExecuteNonQuery() >= 1)
                    {
                        MessageBox.Show("Uspesno azurirano!");
                    }
                    else
                    {
                        throw new Exception("Doslo je do greske, nista nije azurirano");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }

                addIDs();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select * from turisticka_destinacija where turisticka_destinacija.destinacijaID = @id order by turisticka_destinacija.destinacijaID asc";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = destinationIDs.Max();
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        textBox1.Text = rdr[0].ToString();
                        textBox2.Text = rdr[1].ToString();
                        textBox3.Text = rdr[2].ToString();
                        textBox4.Text = rdr[3].ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }

            }
        }
    }
}
