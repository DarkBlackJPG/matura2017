﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace EIT_B7
{
    public partial class po_aranzmanima : Form
    {
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-4GG1978;Initial Catalog=turisticka_agencija;Integrated Security=True");
        public po_aranzmanima()
        {
            InitializeComponent();
        }

        public void refresh()
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "select distinct hotel.kategorija from hotel order by hotel.kategorija asc ";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    comboBox1.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "select distinct hotel.drzava from hotel order by hotel.drzava asc ";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
       
        private void po_aranzmanima_Load(object sender, EventArgs e)
        {
            refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime date = new DateTime();
            monthCalendar1.SetDate(date);

            MessageBox.Show(date.ToString());
        }
    }
}
