﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EIT_B7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void destinacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            destinacije form = new destinacije();
            form.ShowDialog();
        }

        private void poAranzmanimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            po_aranzmanima form = new po_aranzmanima();
            form.ShowDialog();
        }
    }
}
