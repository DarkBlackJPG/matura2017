﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class TipAntikviteta : Form
    {
        public TipAntikviteta()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection baza = new SqlConnection();
            baza.ConnectionString = (@"Data Source=dario-pc\sqlexpress;Initial Catalog=EIT-B9;Integrated Security=True");
            try
            {
                SqlCommand dodaj = new SqlCommand("insert Tip_antikviteta (Tip_Antikviteta,Tip) values (@p1,@p2)", baza);
                dodaj.Parameters.AddWithValue("@p1", Convert.ToInt16(textBox1.Text));
                dodaj.Parameters.AddWithValue("@p2", textBox2.Text);
                baza.Open();
                dodaj.Connection = baza;
                dodaj.ExecuteNonQuery();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { 
                baza.Close();
            MessageBox.Show("Uspesno ste dodali u bazu");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection baza = new SqlConnection();
            baza.ConnectionString=(@"Data Source=dario-pc\sqlexpress;Initial Catalog=EIT-B9;Integrated Security=True");
            try
            {
                SqlCommand obrisi = new SqlCommand("delete from Tip_antikviteta where Tip_Antikviteta=@p1", baza);
                obrisi.Parameters.AddWithValue("@p1", Convert.ToInt16(textBox1.Text));
                baza.Open();
                obrisi.Connection = baza;
                obrisi.ExecuteNonQuery();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                baza.Close();
                MessageBox.Show("Uspesno ste obrisali podatke iz baze");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection baza = new SqlConnection();
            baza.ConnectionString = (@"Data Source=dario-pc\sqlexpress;Initial Catalog=EIT-B9;Integrated Security=True");
            try
            {
                SqlCommand izmeni = new SqlCommand("update Tip_antikviteta set Tip=@p1 where Tip_antikviteta=@p2", baza);
                izmeni.Parameters.AddWithValue("@p1", textBox2.Text);
                izmeni.Parameters.AddWithValue("@p2", Convert.ToInt16(textBox1.Text));
                baza.Open();
                izmeni.Connection = baza;
                izmeni.ExecuteNonQuery();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                baza.Close();
                MessageBox.Show("Uspesno ste izmenili podatke");
            }
        }
    }
}
