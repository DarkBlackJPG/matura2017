﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApplication1
{
    public partial class PoArheologu : Form
    {
        SqlConnection baza = new SqlConnection(@"Data Source=dario-pc\sqlexpress;Initial Catalog=EIT-B9;Integrated Security=True");
        int x = 1;
        int y = 1;
        public PoArheologu()
        {
            InitializeComponent();
        }

        private void PoArheologu_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the '_EIT_B9DataSet.Lokalitet' table. You can move, or remove it, as needed.
            this.lokalitetTableAdapter.Fill(this._EIT_B9DataSet.Lokalitet);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Pen p = new Pen(Color.Blue);
            Pen tacka = new Pen(Color.Red, 2);
            e.Graphics.DrawRectangle(p, 0, 0, 360, 180);
            e.Graphics.DrawLine(p, 180, 0, 180, 180);
            e.Graphics.DrawLine(p, 0, 90, 360, 90);

            e.Graphics.DrawEllipse(tacka, x, y, 4, 4);


            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand prikazi = new SqlCommand("Select Lokalitet.Naziv as[Lokalitet],Lokalitet.KoordinateDuzina as[Geografska duzina],Lokalitet.KoordinateSirina as[Geografska sirina] from Arheolog,Antikvitet,Lokalitet where Arheolog.ArheologID=Antikvitet.AntikvitetID AND Antikvitet.AntikvitetID=Lokalitet.LokalitetID AND Arheolog.ArheologID=@p1", baza);
            prikazi.Parameters.AddWithValue("@p1", SqlDbType.Text).Value=textBox1.Text;
            SqlDataAdapter dd=new SqlDataAdapter(prikazi);
            DataSet ds=new DataSet();
            dd.Fill(ds,"urbanizacija");
            dataGridView1.DataSource=ds;
            dataGridView1.DataMember="urbanizacija";
            baza.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            x = 180;
            y = 90;
            try
            {

                 if (dataGridView1.CurrentRow.Cells[1].Value.ToString().Split(' ')[1] == "istocno")
                {
                    x =180- Convert.ToInt32(dataGridView1.CurrentRow.Cells[1].Value.ToString().Split(' ')[0]);
                }
                else
                {
                    
                    x=180+ Convert.ToInt32(dataGridView1.CurrentRow.Cells[1].Value.ToString().Split(' ')[0]);

                }
                if (dataGridView1.CurrentRow.Cells[2].Value.ToString().Split(' ')[1] == "severno")
                {
                    y =90- Convert.ToInt32(dataGridView1.CurrentRow.Cells[2].Value.ToString().Split(' ')[0]);
                }
                else
                {
                    y =90+ Convert.ToInt32(dataGridView1.CurrentRow.Cells[2].Value.ToString().Split(' ')[0]);

                }
              
            }
            catch
            {
                MessageBox.Show("Urbanizacija");
            }
            finally
            {
                pictureBox1.Refresh();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
