﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void krajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tipAntikvitetaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TipAntikviteta aa = new TipAntikviteta();
            aa.ShowDialog();
        }

        private void poArheologuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PoArheologu pp = new PoArheologu();
            pp.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
