﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EITB34
{
    public partial class Golf_klub : Form
    {
        public Golf_klub()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tereniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tereni t = new Tereni();
            t.ShowDialog();
        }

        private void spisakPartijaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Spisak_partija sp = new Spisak_partija();
            sp.ShowDialog();
        }
    }
}
