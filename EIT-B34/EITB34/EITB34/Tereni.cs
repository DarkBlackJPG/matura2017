﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EITB34
{
    public partial class Tereni : Form
    {
        public Tereni()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Tereni_Load(object sender, EventArgs e)
        {
            //ucitavanje Sifara i Gradova prilikom Load-a Dialoga
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            SqlConnection conn2 = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT Grad From Grad ORDER BY Grad", conn);
                SqlCommand cmd2 = new SqlCommand("SELECT TerenID FROM Teren", conn2);
                conn.Open();
                conn2.Open();
                SqlDataReader read = cmd.ExecuteReader();
                SqlDataReader read2 = cmd2.ExecuteReader();
                while (read.Read())
                {
                    comboBox2.Items.Add(read[0].ToString());
                }
                while(read2.Read())
                {
                    comboBox1.Items.Add(read2[0].ToString());
                }
                read.Close();
                read2.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //prikazuju se podaci za odabranu sifru
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            SqlConnection conn2 = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            try
            {
                string s = comboBox1.Text;
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT Teren.Teren, Teren.Adresa, Teren.KontaktTelefon, Grad.GradID FROM Teren,Grad WHERE Teren.GradID=Grad.GradID AND Teren.TerenID="+s, conn);                
                SqlDataReader read3 = cmd.ExecuteReader();
                read3.Read();
                string k = read3[3].ToString();
                //u k-u se nalazi koji je GradID za izabrani Teren
                SqlCommand cmd2 = new SqlCommand("SELECT Grad From Grad WHERE GradID=" + k, conn2);
                conn2.Open();
                SqlDataReader read = cmd2.ExecuteReader();
                read.Read();
                comboBox2.SelectedItem = read[0].ToString();
                    textBox1.Text = read3[0].ToString();
                    textBox2.Text = read3[1].ToString();
                    textBox3.Text = read3[2].ToString();

                    read.Close();
                read3.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //unosenje podataka, radi se sa 2 cmda zbog unosenja GradID-a
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            SqlConnection conn2 = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT GradID FROM Grad WHERE Grad='" + comboBox2.Text.ToString()+"'", conn2);
                conn2.Open();
                SqlDataReader read = cmd2.ExecuteReader();
                read.Read();
                SqlCommand cmd = new SqlCommand("INSERT INTO Teren (Teren,Adresa,KontaktTelefon,GradID) VALUES ('" + textBox1.Text+"', '" + textBox2.Text + "', '" + textBox3.Text + "', " + Convert.ToInt32(read[0]) + ")",conn);
                
                conn.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Uspesno ste upisali podatke");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                conn2.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");

            try
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM Teren WHERE TerenID="+ comboBox1.Text.ToString(), conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                comboBox1.Text = "";
                textBox1.Clear();
                textBox2.Clear();
                textBox3.Clear();
                MessageBox.Show("Uspesno ste izbrisali podatak");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //2 cmda zbog GradID-a
            //u Teren tabeli upisujemo GradID, ali u comboboxu biramo ime Grada a ne ID, zato pristupam u obe tabele
            SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            SqlConnection conn2 = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && comboBox2.Text != "")
            {
                try
                {
                    SqlCommand cmd2 = new SqlCommand("SELECT GradID FROM Grad WHERE Grad='" + comboBox2.Text.ToString()+"'", conn2);
                    conn2.Open();
                    SqlDataReader read = cmd2.ExecuteReader();
                    read.Read();
                    
                    SqlCommand cmd = new SqlCommand("UPDATE Teren SET Teren='" + textBox1.Text + "', Adresa='" + textBox2.Text + "', KontaktTelefon='" + textBox3.Text+ "', GradID="+Convert.ToInt32(read[0])+" WHERE TerenID="+ comboBox1.Text.ToString(), conn);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Uspesno ste izmenili podatke");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    conn.Close();
                    conn2.Close();
                }
            }
            else
            {
                MessageBox.Show("Popuni polja prvo :))))");
            }
        }
    }
}
