﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EITB34
{
    public partial class Spisak_partija : Form
    {
        SqlConnection conn2 = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
        SqlConnection conn = new SqlConnection(@"Data Source=DJOKO-PC\SQLEXPRESS;Initial Catalog=Golf_klub;Integrated Security=True");
        List<int> pocetak = new List<int>();
        List<int> zavrsetak = new List<int>();
        List<int> lista = new List<int>();
        
        public Spisak_partija()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dt.Columns.Count == 0)
                {
                    dt.Columns.Add("Trajanje/min");
                    dt.Columns.Add("Teren");
                    dt.Columns.Add("Partija");
                    dt.Columns.Add("Datum");
                }
                conn.Open();
                int a = Convert.ToInt32(comboBox1.Text);
                int b = Convert.ToInt32(comboBox2.Text);
                int rezultat = 0;
                //provera zbog 24-casovnog formata
                if (a > b)
                {
                    rezultat = 24 - (a - b);
                }
                else
                {
                    rezultat = Math.Abs(a - b);
                }
                int i = 0;
                while(rezultat>=0)
                {
                    if (a + i == 24)
                    {
                        a = 0;
                        i = 0;
                    }
                SqlCommand cmd = new SqlCommand("SELECT PartijaID FROM Partija WHERE DATEPART(hour,VremePocetka)="+(a+i).ToString(), conn);
                SqlDataReader read = cmd.ExecuteReader();
                while(read.Read())
                {
                    pocetak.Add(Convert.ToInt32(read[0]));
                }
                read.Close();   
                cmd.CommandText = "SELECT PartijaID FROM Partija WHERE DATEPART(hour,VremeZavrsetka)="+(a+i).ToString();
                read = cmd.ExecuteReader();
                while(read.Read())
                {
                    zavrsetak.Add(Convert.ToInt32(read[0]));
                }
                    rezultat--;
                    i++;
                    read.Close();
                }
                //pronalazi da li u odredjenom opsegu brojeva postoje odigrane partije
                //ako postoje, onda u for petlji ispituje da li postoje neke koje imaju isti ID
                //tj. da li spadaju u istu partiju
                for (int k = 0; k < pocetak.Count; k++ )
                    for (int j = 0; j < zavrsetak.Count; j++)
                    {
                        if (pocetak[k] == zavrsetak[j])
                        {
                            lista.Add(pocetak[k]);
                        }
                    }
                //nakon for-a imam sve ID-eve partija (u listi), koje su odigrane u odgovarajucem opsegu vremena
                //sada mi je samo preostalo da ispisem podatke u tabelu sledecom for petljom
                conn2.Open();
                for (int z = 0; z < lista.Count; z++)
                {
                    SqlCommand cmd2 = new SqlCommand("SELECT Teren.Teren,Partija.PartijaID,CONVERT(VARCHAR(10), Partija.Datum, 111) FROM Partija,Teren WHERE Partija.PartijaID=" + lista[z].ToString()+" AND Teren.TerenID=Partija.TerenID", conn2);
                    SqlDataReader read2 = cmd2.ExecuteReader();
                    read2.Read();
                    //u read2 imam poslednje tri kolone, u read3 cu ubaciti trajanje partije
                    //formula koju sam izmislio za izracunavanje vremena trajanja:
                    // ||H1-H2|*60-|M1-M2|| = x minuta
                    //ako je H1>H2 onda (24-(H1-H2))*60 = x; x-(M1-M2) = vreme trajanja
                    conn.Close();
                    conn.Open();
                    SqlCommand cmd3 = new SqlCommand("SELECT DATEPART(hour, VremePocetka),DATEPART(hour, VremeZavrsetka),DATEPART(minute, VremePocetka),DATEPART(minute, VremeZavrsetka) FROM Partija WHERE PartijaID="+lista[z].ToString(),conn);
                    SqlDataReader read3 = cmd3.ExecuteReader();
                    while (read3.Read())
                    {
                        int H1 = Convert.ToInt32(read3[0]);
                        int H2 = Convert.ToInt32(read3[1]);
                        int M1 = Convert.ToInt32(read3[2]);
                        int M2 = Convert.ToInt32(read3[3]);
                        int vreme = 0;
                        int x = 0;
                        if (H1 > H2)
                        {
                            x = (24 - (H1 - H2)) * 60;
                            vreme = x - (M1 - M2);
                        }
                        else
                        {
                            vreme = Math.Abs(Math.Abs(H1 - H2) * 60 - Math.Abs(M1 - M2));
                        }
                        dt.Rows.Add(vreme.ToString(), read2[0], read2[1], read2[2]);
                    }
                    read2.Close();
                    read3.Close();
                }
                dataGridView1.DataSource = dt;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                pocetak.Clear();
                zavrsetak.Clear();
                lista.Clear();
                conn.Close();
                conn2.Close();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow selectedrow = dataGridView1.Rows[e.RowIndex];
                string ID = selectedrow.Cells[2].Value.ToString();
                SqlCommand cmd = new SqlCommand("SELECT Teren.Teren, Teren.Adresa,Teren.GradID,Teren.KontaktTelefon FROM Teren,Partija WHERE Partija.TerenID=Teren.TerenID AND Partija.PartijaID=" + ID, conn);
                conn.Open();
                SqlDataReader read = cmd.ExecuteReader();
                read.Read();
                SqlCommand cmd2 = new SqlCommand("SELECT Grad FROM Grad WHERE Grad.GradID=" + read[2].ToString(),conn2);
                conn2.Open();
                SqlDataReader read2 = cmd2.ExecuteReader();
                read2.Read();
                label5.Text = read[0].ToString() + ", " + read[1].ToString() + ", " + read2[0].ToString() + ", " + read[3].ToString();
                label5.Visible = true;
                read.Close();
                read2.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                conn2.Close();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {

           
        }
    }
}
