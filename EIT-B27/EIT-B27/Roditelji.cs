﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace EIT_B27
{
    public partial class Roditelji : Form
    {
        SqlConnection konekcija = new SqlConnection(@"Data Source =.\sqlexpress; Initial Catalog = Boravak_dece; Integrated Security = True");

        public Roditelji()
        {
            InitializeComponent();
        }

        public void Roditelji_Load(object sender, EventArgs e)
        {
            int x = Convert.ToInt32(textBox1.Text);

            if (x == 1)
            {
                button5.Enabled = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void button2_Click(object sender, EventArgs e)
        {
            
            //Za ID increment
            int x = int.Parse(textBox1.Text);
            x++;
            textBox1.Text = x.ToString();

            //Clear
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";

            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;



        }

        private void button3_Click(object sender, EventArgs e)
        {


             

            if (radioButton1.Checked == true)
            {
                try
                {



                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = konekcija;
                    cmd.CommandText = "INSERT INTO Roditelj(RoditeljID, SvojstvoID, Ime, Prezime, Adresa, FiksniTelefon, MobilniTelefon) values (@id, 1, @ime, @prezime, @adresa, @fiks, @mob)";

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                    cmd.Parameters.Add("@ime", SqlDbType.VarChar).Value = textBox2.Text;
                    cmd.Parameters.Add("@prezime", SqlDbType.VarChar).Value = textBox3.Text;
                    cmd.Parameters.Add("@adresa", SqlDbType.VarChar).Value = textBox4.Text;
                    cmd.Parameters.Add("@fiks", SqlDbType.VarChar).Value = textBox5.Text;
                    cmd.Parameters.Add("@mob", SqlDbType.VarChar).Value = textBox6.Text;

                    konekcija.Open();
                    cmd.ExecuteNonQuery();
                    

                  }

                catch (Exception ex)
                {
                    MessageBox.Show("Nesto ne valja");

                }

                finally

                {
                    konekcija.Close();
                }
              }

            if (radioButton2.Checked == true)
            {

                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = konekcija;
                    cmd.CommandText = "INSERT INTO Roditelj(RoditeljID, SvojstvoID, Ime, Prezime, Adresa, FiksniTelefon, MobilniTelefon) values (@id, 2, @ime, @prezime, @adresa, @fiks, @mob)";

                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                    cmd.Parameters.Add("@ime", SqlDbType.VarChar).Value = textBox2.Text;
                    cmd.Parameters.Add("@prezime", SqlDbType.VarChar).Value = textBox3.Text;
                    cmd.Parameters.Add("@adresa", SqlDbType.VarChar).Value = textBox4.Text;
                    cmd.Parameters.Add("@fiks", SqlDbType.VarChar).Value = textBox5.Text;
                    cmd.Parameters.Add("@mob", SqlDbType.VarChar).Value = textBox6.Text;

                    konekcija.Open();
                    cmd.ExecuteNonQuery();
                    konekcija.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("Nesto ne valja");

                }

                finally

                {
                    konekcija.Close();
                }
            }

                if (radioButton3.Checked == true)
                {
                    try
                    {

                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = konekcija;
                        cmd.CommandText = "INSERT INTO Roditelj(RoditeljID, SvojstvoID, Ime, Prezime, Adresa, FiksniTelefon, MobilniTelefon) values (@id, 3, @ime, @prezime, @adresa, @fiks, @mob)";

                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                        cmd.Parameters.Add("@ime", SqlDbType.VarChar).Value = textBox2.Text;
                        cmd.Parameters.Add("@prezime", SqlDbType.VarChar).Value = textBox3.Text;
                        cmd.Parameters.Add("@adresa", SqlDbType.VarChar).Value = textBox4.Text;
                        cmd.Parameters.Add("@fiks", SqlDbType.VarChar).Value = textBox5.Text;
                        cmd.Parameters.Add("@mob", SqlDbType.VarChar).Value = textBox6.Text;

                        konekcija.Open();
                        cmd.ExecuteNonQuery();
                        konekcija.Close();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("Nesto ne valja");

                    }

                    finally

                    {
                        konekcija.Close();
                    }
                }
                
            
            






        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            button5.Enabled = true;

            int x = int.Parse(textBox1.Text);
            x++;
            textBox1.Text = x.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = konekcija;
            cmd.CommandText = "Select * from Roditelj where RoditeljID = @id";
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(textBox1.Text);
            konekcija.Open();
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {

                int svojstvo = Convert.ToInt32(rdr[1]);

                if (svojstvo == 1)
                {
                    radioButton1.Checked = true;
                }

                if (svojstvo == 2)
                {
                    radioButton2.Checked = true;
                }

                if (svojstvo == 3)
                {
                    radioButton3.Checked = true;
                }


                textBox2.Text = rdr[2].ToString();
                textBox3.Text = rdr[3].ToString();
                textBox4.Text = rdr[4].ToString();
                textBox5.Text = rdr[5].ToString();
                textBox6.Text = rdr[6].ToString();
            }

            konekcija.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            

            int x = int.Parse(textBox1.Text);
            x--;
            textBox1.Text = x.ToString();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = konekcija;
            cmd.CommandText = "Select * from Roditelj where RoditeljID = @id";
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = Convert.ToInt32(textBox1.Text);
            konekcija.Open();
            SqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                int svojstvo = Convert.ToInt32(rdr[1]);

                if (svojstvo == 1)
                {
                    radioButton1.Checked = true;
                }
                if (svojstvo == 2)
                {
                    radioButton2.Checked = true;
                }

                if (svojstvo == 3)
                {

                    radioButton3.Checked = true;
                }

                textBox2.Text = rdr[2].ToString();
                textBox3.Text = rdr[3].ToString();
                textBox4.Text = rdr[4].ToString();
                textBox5.Text = rdr[5].ToString();
                textBox6.Text = rdr[6].ToString();

                
            }

            konekcija.Close();


            if (textBox1.Text == "1")
            {
                button5.Enabled = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Point tacka = new Point(0, 250);
            Pen olovka = new Pen(System.Drawing.Color.Blue);
            Pen olovka1 = new Pen(System.Drawing.Color.Green);
            Pen olovka2 = new Pen(System.Drawing.Color.Red);
            System.Drawing.Graphics canvas = this.CreateGraphics();
            canvas.DrawEllipse(olovka, new Rectangle(325, 50, 50, 50));
            canvas.DrawRectangle(olovka1, new Rectangle(300, 100, 100, 100));
            canvas.DrawLine(olovka2, 300, 100, 215, 150);
            canvas.DrawLine(olovka2, 400, 100, 475, 60);
            canvas.DrawLine(olovka2, 375, 200, 375, 275);
            canvas.DrawLine(olovka2, 325, 200, 325, 275);
        }
    }
}
