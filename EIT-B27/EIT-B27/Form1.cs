﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EIT_B27
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void roditeljiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Roditelji form1 = new Roditelji();
            form1.ShowDialog();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void brojDeceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Broj_dece form1 = new Broj_dece();
            form1.ShowDialog();
        }
    }
}
