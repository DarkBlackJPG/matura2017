﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EIT_B36
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ulovToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ulov form = new Ulov();
            form.ShowDialog();
        }

        private void pretragaOpisaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PretragaOpisa form = new PretragaOpisa();
            form.ShowDialog();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
