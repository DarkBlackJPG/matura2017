﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EIT_B36
{
    public partial class Ulov : Form
    {
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-B36;Integrated Security=True");

        public Ulov()
        {
            InitializeComponent();
        }
        public void refreshNumber()
        {
            if (radioButton1.Checked)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select MAX(RbrUlova) from Ulov";
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                        textBox1.Text = (Convert.ToInt32(rdr[0]) + 1).ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
           
        }

        public void checkRecords(string pecarosID, string date, string time, string fish, out string redniBroj)
        {
            string value = "";
            if (pecarosID != "" && date != "" && time != "" && fish != "")
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select RbrUlova from Ulov where PecarosID = @id and Datum = @date and Vreme = @time and VrstaID = (Select VrstaID from Vrsta_Ribe where Naziv = @fish)";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = pecarosID;
                    cmd.Parameters.Add("@date", SqlDbType.Date).Value = date;
                    cmd.Parameters.Add("@time", SqlDbType.Time).Value = time;
                    cmd.Parameters.Add("@fish", SqlDbType.VarChar).Value = fish;
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if(rdr.HasRows)
                    {
                        while(rdr.Read())
                        {
                            value = rdr[0].ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
            else
                value = "";
            redniBroj = value;
        }

        private void Ulov_Load(object sender, EventArgs e)
        {
            
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select PecarosID, Ime, Prezime from Pecaros";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    comboBox1.Items.Add(rdr[0].ToString() + " - " + rdr[1].ToString() + " " + rdr[2].ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select Naziv from Vrsta_Ribe";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }

            refreshNumber();
           
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

            if (radioButton1.Checked == true)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Select MAX(RbrUlova) from Ulov";
                    _CONN.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                        textBox1.Text = (Convert.ToInt32(rdr[0])+1).ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
        }

        public string formatDate(string x)
        {
            string[] array = x.Split('/');
            return (array[2] + "-" + array[1] + "-" + array[0]);
        }
        public string getID(string id)
        {
            string[] array = id.Split('-');
            return array[0];
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
            {
                try
                {
                    string[] array = comboBox1.Text.Split('-');
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "INSERT INTO Ulov(PecarosID, Datum, Vreme, VrstaID) VALUES(@id, @date, @time, (Select Vrsta_Ribe.VrstaID from Vrsta_Ribe where Vrsta_Ribe.Naziv = @fish))";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = array[0];
                    cmd.Parameters.Add("@date", SqlDbType.Date).Value = maskedTextBox1.Text;
                    cmd.Parameters.Add("@time", SqlDbType.Time).Value =maskedTextBox2.Text;
                    cmd.Parameters.Add("@fish", SqlDbType.VarChar).Value = comboBox2.Text;
                    _CONN.Open();
                    if (cmd.ExecuteNonQuery() >= 1)
                    {
                        MessageBox.Show("Uspesno Dodato!");
                    }
                    else
                        throw new Exception("Doslo je do problema");


                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                    refreshNumber();
                }
            }
            else if(radioButton2.Checked)
            {
                try
                {
                    if (textBox1.Text == "")
                        throw new Exception("Ne postoji ovakav unos!");
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "Delete from Ulov where PecarosID = @id and RbrUlova = @rbr";
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = getID(comboBox1.Text);
                    cmd.Parameters.Add("@rbr", SqlDbType.Int).Value = textBox1.Text;
                    _CONN.Open();
                    if (cmd.ExecuteNonQuery() >= 1)
                    {
                        MessageBox.Show("Uspesnp izbrisano");
                    }
                    else
                        MessageBox.Show("Doslo je do problema");
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }

        private void maskedTextBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }

        private void maskedTextBox2_TextChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                string number = "";
                checkRecords(getID(comboBox1.Text), maskedTextBox1.Text, maskedTextBox2.Text, comboBox2.Text, out number);
                textBox1.Text = number;
            }
        }
    }
}
