﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace EIT_B36
{
    public partial class PretragaOpisa : Form
    {
        public PretragaOpisa()
        {
            InitializeComponent();
        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-B36;Integrated Security=True");
        private void PretragaOpisa_Load(object sender, EventArgs e)
        {
            checkBox1.Text = "Razlikuje velika i\n mala slova";
            checkBox2.Text = "Pronadji samo\n citave reci";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        class fishObject
        {
            string id, name, description;
            public string ID
            {
                get { return id; }
            }
            public string Name
            {
                get { return name; }
            }
            public string Description
            {
                get { return description; }
            }

            public void fillAttributes(string id, string name, string description)
            {
                this.id = id;
                this.name = name;
                this.description = description;
            }
        }
        List<fishObject> fishArray = new List<fishObject>();
        private void button1_Click(object sender, EventArgs e)
        {
            
            if(checkBox1.Checked)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = _CONN;
                    cmd.CommandText = "SELECT Vrsta_Ribe.VrstaID as Sifra, Vrsta_Ribe.Naziv, Vrsta_Ribe.Opis from Vrsta_Ribe WHERE Vrsta_Ribe.Opis like '%"+textBox1.Text+ "%' COLLATE Latin1_General_CS_AS";
                    _CONN.Open();
                    SqlDataAdapter adr = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adr.Fill(dt);
                    dataGridView1.DataSource = dt;
                    
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    _CONN.Close();
                }
            }


            /** NE ZNAM KAKVE VEZE IMA OVAJ DRUGI CHECKBOX!?!?!?!?!
            *
            *
            **/
            //if(checkBox2.Checked)
            //{
            //    try
            //    {
            //        SqlCommand cmd = new SqlCommand();
            //        cmd.Connection = _CONN;
            //        cmd.CommandText = "SELECT Vrsta_Ribe.VrstaID as Sifra, Vrsta_Ribe.Naziv, Vrsta_Ribe.Opis from Vrsta_Ribe WHERE Vrsta_Ribe.Opis like '%" + textBox1.Text + "%'";
            //        _CONN.Open();
            //        SqlDataReader rdr = cmd.ExecuteReader();
            //        while(rdr.Read())
            //        {
            //            fishObject fishX = new fishObject();
            //            fishX.fillAttributes(rdr[0].ToString(), rdr[1].ToString(), rdr[2].ToString());
            //            fishArray.Add(fishX);
            //        }

            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //    }
            //    finally
            //    {
            //        _CONN.Close();
            //    }
            //    DataTable dt = new DataTable();
            //    foreach (fishObject x in fishArray)
            //    {
                    
            //        if(Regex.Match(x.Description, @"\b"+textBox1.Text+@"\b").Success)
            //        {
                        
            //            DataRow drw = dt.NewRow();
            //            drw["Sifra"] = x.ID;
            //            drw["Naziv"] = x.Name;
            //            drw["Opis"] = x.Description;
            //            dt.Rows.Add(drw);
            //        }
            //    }
            //    dataGridView1.DataSource = dt;
            //}
        }
    }
}
