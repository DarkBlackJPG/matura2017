# README  #

Ovde ce manje-vise sve biti objasnjeno kako i sta treba odraditi.

## 1. Kako skinuti fajlove? ##

Fajlovi se lako mogu skinuti.
Dovoljno je otici u [Downloads](https://bitbucket.org/DarkBlackJPG/matura2017/downloads/) iz levog menija. Posle toka, ispod Name-a ima "Download repository".

## 2. Ako necete da skinete... ##

...levi meni, onda [Source](https://bitbucket.org/DarkBlackJPG/matura2017/src) i tamo odete do tog-i-tog fajl-a.

### Napomena: ###

Projekti su radjeni u razlicitim verzijama Visual Studia. Ako zelite neki da otvorite, a ne zeli da se otvori - moracete vise da se oslanjate na citanje samog koda...

### *Stefan Teslic* ###