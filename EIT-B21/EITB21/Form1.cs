﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EITB21
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radnaMestaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Radnamesta forma = new Radnamesta();
            forma.ShowDialog();
        }

        private void nezavrseniProjektiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Nezavrseniprojekti forma1 = new Nezavrseniprojekti();
            forma1.ShowDialog();
        }
    }
}
