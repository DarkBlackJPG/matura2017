﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Pozorisne_Predstave
{
    public partial class Form3 : Form
    {
        SqlConnection konekcija = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=Pozorisne_predstave;Integrated Security=True");
        public Form3()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string s = comboBox1.SelectedItem.ToString();
            SqlCommand cmd = new SqlCommand("Select Glumac.GlumacID, Glumac.Ime, Glumac.Prezime from Glumac, Pozorisna_Trupa where Pozorisna_Trupa.TrupaID=Glumac.TrupaID and Pozorisna_Trupa.NazivTrupe=@p ORDER BY Glumac.GlumacID ASC;", konekcija);
            cmd.Parameters.AddWithValue("@p", s);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = konekcija;
            konekcija.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            konekcija.Close();
            ///
            SqlCommand cmd1 = new SqlCommand("select COUNT(Pozorisni_Komad.KomadID)as 'Odigrano puta', Pozorisni_Komad.Trajanje, Pozorisni_Komad.Naziv from Pozorisni_Komad, Predstava, Pozorisna_Trupa where Pozorisni_Komad.KomadID=Predstava.KomadID and Predstava.TrupaID = Pozorisna_Trupa.TrupaID and Pozorisna_Trupa.NazivTrupe=@p  group by Pozorisni_Komad.Naziv, Pozorisni_Komad.Trajanje ;", konekcija);
            cmd1.Parameters.AddWithValue("@p", s);
            cmd1.CommandType = CommandType.Text;
            cmd1.Connection = konekcija;
            konekcija.Open();
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView2.DataSource = dt1;
            konekcija.Close();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Select NazivTrupe from Pozorisna_Trupa;", konekcija);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = konekcija;

            konekcija.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                comboBox1.Items.Add(reader["NazivTrupe"].ToString());
            }

            konekcija.Close();
        }
    }
}
