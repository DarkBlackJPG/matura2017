﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Pozorisne_Predstave
{
    public partial class Form2 : Form
    {
        SqlConnection konekcija = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=Pozorisne_predstave;Integrated Security=True");
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Select KomadID, Naziv from Pozorisni_Komad;",konekcija);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = konekcija;

            konekcija.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                comboBox1.Items.Add(reader["KomadID"].ToString() + "  " + reader["Naziv"].ToString());
            }

            konekcija.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Select Stavke_Rezervacije.Datum, Pretplatnik.Ime, Pretplatnik.Prezime, Stavke_Rezervacije.RBR from Stavke_Rezervacije, Pretplatnik, Rezervacija where Pretplatnik.PretplatnikID=Rezervacija.PretplatnikID and Rezervacija.RezervacijaID=Stavke_Rezervacije.RezervacijaID and Rezervacija.RezervacijaID="+textBox1.Text + ";", konekcija);
            cmd.CommandType = CommandType.Text;
            cmd.Connection = konekcija;

            konekcija.Open();
            int rbr=000;
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                label2.Text = reader[0].ToString() + "  " + reader[1].ToString() + "  " + reader[2].ToString();
                rbr = Convert.ToInt32(reader[3]);
                textBox2.Text = rbr.ToString();
            }
            konekcija.Close();

            
        }
    }
}
