﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace EIT_B4
{
    public partial class Producenti : Form
    {
        public Producenti()
        {
            InitializeComponent();
        }
        List<producentiClass> producentiArray = new List<producentiClass>();
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-B4;Integrated Security=True");
        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void Refreshes()
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            producentiArray.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select producentID, ime, email from producent";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    producentiClass newProducent = new producentiClass(rdr[1].ToString(), rdr[2].ToString(), Convert.ToInt32(rdr[0].ToString()));
                    producentiArray.Add(newProducent);
                    listBox1.Items.Add(rdr[0].ToString());
                    listBox2.Items.Add(rdr[1].ToString());
                    listBox3.Items.Add(rdr[2].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }

        }
        private void Producenti_Load(object sender, EventArgs e)
        {
            Refreshes();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.SelectedIndex = listBox1.SelectedIndex;
            listBox3.SelectedIndex = listBox1.SelectedIndex;
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = listBox2.SelectedIndex;
            listBox3.SelectedIndex = listBox2.SelectedIndex;
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = listBox3.SelectedIndex;
            listBox2.SelectedIndex = listBox3.SelectedIndex;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox2.Text = "";
            textBox3.Text = "";
            int counter = 0;
            foreach(producentiClass x in producentiArray)
            {
                if(textBox1.Text != "" && x.Id == Convert.ToInt32(textBox1.Text))
                {
                    textBox2.Text = x.Ime;
                    textBox3.Text = x.Email;
                    counter++;
                }
            }
            if(counter <= 0)
            {
                textBox2.Text = "";
                textBox3.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Insert into producent(producentID, ime, email) values (@id, @name, @email)";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = textBox2.Text;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = textBox3.Text;
                _CONN.Open();
                if (MessageBox.Show("Da li ste sigurni?", "Paznja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (cmd.ExecuteNonQuery() >= 1)
                    {
                        MessageBox.Show("Uspesno uneto!");
                    }
                    else
                        MessageBox.Show("Doslo je do nekih problema");
                }
                }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
                Refreshes();
            }
           }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach(producentiClass x in producentiArray)
            {
                if(x.Id == Convert.ToInt32(textBox1.Text) )
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = _CONN;
                        cmd.CommandText = "Delete from producent where producent.producentID = @id";
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                        _CONN.Open();

                        if (MessageBox.Show("Da li ste sigurni?", "Paznja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            if (cmd.ExecuteNonQuery() >= 1)
                            {
                                MessageBox.Show("Uspesno obrisano!");
                            }
                            else
                                MessageBox.Show("Doslo je do nekih problema");
                        }

                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        _CONN.Close();
                        Refreshes();
                       
                    }
                    break;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach(producentiClass x in producentiArray)
            {
                if (x.Id == Convert.ToInt32(textBox1.Text))
                {
                    try {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = _CONN;
                        cmd.CommandText = "Update producent set ime = @name, email = @email where producentID = @id";
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = textBox1.Text;
                        cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = textBox2.Text;
                        cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = textBox3.Text;
                        _CONN.Open();
                        if (MessageBox.Show("Da li ste sigurni?", "Paznja", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            if (cmd.ExecuteNonQuery() >= 1)
                            {
                                MessageBox.Show("Uspesno azurirano!");
                            }
                            else
                                MessageBox.Show("Doslo je do nekih problema");
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        _CONN.Close();
                        Refreshes();
                    }
                    break;
                   }
            }
        }
    }
}
