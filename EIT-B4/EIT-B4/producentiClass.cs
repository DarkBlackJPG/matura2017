﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EIT_B4
{
    class producentiClass
    {
        string ime, email;
        int id;
        public producentiClass()
        {

        }
        public producentiClass(string a, string b, int c)
        {
            ime = a;
            email = b;
            id = c;
        }

        
        public string Ime
        {
            get
            {
                return ime;
            }
        }
        public string Email
        {
            get
            {
                return email;
            }
        }
        public int Id
        {
            get
            {
                return id;
            }
        }

    }
}
