﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace EIT_B4
{
    public partial class VrstaUlogeIproducenti : Form
    {
        public VrstaUlogeIproducenti()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void Refreshes()
        {
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select tip from tip_Uloge";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox1.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "Select ime from producent";
                _CONN.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    comboBox2.Items.Add(rdr[0].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
        SqlConnection _CONN = new SqlConnection(@"Data Source=DESKTOP-K334OMT\MSSQLSERVERV2;Initial Catalog=EIT-B4;Integrated Security=True");
        private void VrstaUlogeIproducenti_Load(object sender, EventArgs e)
        {
            Refreshes();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.SelectedIndex = listBox1.SelectedIndex;
            listBox3.SelectedIndex = listBox1.SelectedIndex;
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = listBox2.SelectedIndex;
            listBox3.SelectedIndex = listBox2.SelectedIndex;
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.SelectedIndex = listBox3.SelectedIndex;
            listBox2.SelectedIndex = listBox3.SelectedIndex;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            try {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = _CONN;
                cmd.CommandText = "select film.naziv, zanr.nazivZanra, glumac.ime, glumac.prezime from glumac, uloga, tip_Uloge, film, zanr, producirao, producent where glumac.glumacID = uloga.glumacID and uloga.tipUlogeID = tip_Uloge.tipUlogeID and tip_Uloge.tip = @uloga and uloga.filmID = film.filmID and film.zanrID = zanr.zanrID and producirao.filmID = film.filmID and producirao.producentID = producent.producentID and producent.ime = @producent";
                cmd.Parameters.Add("@uloga", SqlDbType.VarChar).Value = comboBox1.Text;
                cmd.Parameters.Add("@producent", SqlDbType.VarChar).Value = comboBox2.Text;
                _CONN.Open();
                
                SqlDataReader rdr = cmd.ExecuteReader();
                if (!rdr.HasRows)
                    MessageBox.Show("Ne postoji veza sa unetim parametima");
                while(rdr.Read())
                {
                    listBox1.Items.Add(rdr[0].ToString());
                    listBox2.Items.Add(rdr[1].ToString());
                    listBox3.Items.Add(rdr[2].ToString() + " " + rdr[3].ToString());
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                _CONN.Close();
            }
        }
    }
}
