﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EIT_B4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void glumciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void producentiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Producenti from = new Producenti();
            from.ShowDialog();
        }

        private void poTipuUlogeIProducentuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VrstaUlogeIproducenti from = new VrstaUlogeIproducenti();
            from.ShowDialog();
        }
    }
}
