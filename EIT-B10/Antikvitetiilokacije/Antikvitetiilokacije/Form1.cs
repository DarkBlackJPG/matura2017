﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Antikvitetiilokacije
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void periodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Periodi p = new Periodi();
            p.ShowDialog();
        }

        private void poTipuAntikvitetaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Potipu PP = new Potipu();
                PP.ShowDialog();
        }
    }
}
