﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Antikvitetiilokacije
{
    public partial class Potipu : Form
    {
        int x=-4;
        int y=-4;
        SqlConnection konekcija = new SqlConnection(@"Data Source=Puzovic-PC\SQLEXPRESS;Initial Catalog=Antikviteti;Integrated Security=True");
        public Potipu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            x =-4;
            y = -4;
            string b = "Uspesna pretraga";
            try
            {
                SqlCommand komanda = new SqlCommand("SELECT Lokaliteti.naziv,Lokaliteti.kordinataduzina,Lokaliteti.kordinatasirina FROM Lokaliteti,Antikvitet,Tip WHERE Tip.id=Antikvitet.tipid AND Antikvitet.lokalitetid=Lokaliteti.id AND Tip.tip='" + textBox1.Text + "'", konekcija);
                konekcija.Open();
                SqlDataAdapter da = new SqlDataAdapter(komanda);
                DataSet ds = new DataSet();
                da.Fill(ds, "POTIPU");
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "POTIPU";
                if (dataGridView1.Rows[0].Cells[0].Value==null)
                {
                 
                    b = "Nema rezultata";
                }
            }
   
            
            finally
            {
                konekcija.Close();
                MessageBox.Show(b);
                pictureBox1.Refresh();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            x = 180;
            y = 90;
            try
            {

                 if (dataGridView1.CurrentRow.Cells[1].Value.ToString().Split(' ')[1] == "istocno")
                {
                    x =180- Convert.ToInt32(dataGridView1.CurrentRow.Cells[1].Value.ToString().Split(' ')[0]);
                }
                else
                {
                    
                    x=180+ Convert.ToInt32(dataGridView1.CurrentRow.Cells[1].Value.ToString().Split(' ')[0]);

                }
                if (dataGridView1.CurrentRow.Cells[2].Value.ToString().Split(' ')[1] == "severno")
                {
                    y =90- Convert.ToInt32(dataGridView1.CurrentRow.Cells[2].Value.ToString().Split(' ')[0]);
                }
                else
                {
                    y =90+ Convert.ToInt32(dataGridView1.CurrentRow.Cells[2].Value.ToString().Split(' ')[0]);

                }
              
            }
            catch
            {
                MessageBox.Show("Izaberite lokalitet");
            }
            finally
            {
                pictureBox1.Refresh();
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Pen okvir=new Pen(Color.Blue,4);
            Pen okvir2 = new Pen(Color.Blue, 2);
            Pen tacka = new Pen(Color.Red, 4);
            e.Graphics.DrawRectangle(okvir, 0, 0, 360, 180);
            e.Graphics.DrawLine(okvir2, 180, 0, 180, 180);
            e.Graphics.DrawLine(okvir2, 0, 90, 360, 90);

            e.Graphics.DrawEllipse(tacka, x, y, 4, 4);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
