﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Antikvitetiilokacije
{
    public partial class Periodi : Form
    {
        SqlConnection konekcija = new SqlConnection(@"Data Source=Puzovic-PC\SQLEXPRESS;Initial Catalog=Antikviteti;Integrated Security=True");
        public Periodi()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != "")
                {
                    SqlCommand kom = new SqlCommand("SELECT * from Period WHERE Period.id=" + textBox1.Text, konekcija);
                    konekcija.Open();
                    SqlDataReader rd = kom.ExecuteReader();
                    textBox2.Text = "";
                    button1.Enabled = true;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    while (rd.Read())
                    {
                        button1.Enabled = false;
                        button2.Enabled = true;
                        button3.Enabled = true;
                        textBox1.Text = rd[0].ToString();
                        textBox2.Text = rd[1].ToString();

                    }
                }
                else
                {
                    textBox2.Text = "";
                    button1.Enabled = true;
                    button2.Enabled = false;
                    button3.Enabled = false;
                }
            }
            catch 
            {
               
                MessageBox.Show("Pogresno unesene vrednosti");
            }
                
            finally
            {
                konekcija.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string b="Uspesno upisano";
            try
            {
                SqlCommand upisi = new SqlCommand("INSERT INTO Period VALUES(" + textBox1.Text + ",'" + textBox2.Text + "')", konekcija);
                konekcija.Open();
                upisi.ExecuteNonQuery();
               
            }
            catch 
            {
                MessageBox.Show("Pogresno unesene vrednosti");
                b = "Neuspesno upisano";
               
            }

            finally
            {
                MessageBox.Show(b);
                konekcija.Close();
                button1.Enabled = false;
                button2.Enabled = true;
                button3.Enabled = true;
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                SqlCommand izbrisi = new SqlCommand("DELETE from Period where Period.id=" + textBox1.Text, konekcija);
                konekcija.Open();
                izbrisi.ExecuteNonQuery();
                
                textBox1.Text = "";
                textBox2.Text = "";
            }
       
            finally
            {
                MessageBox.Show("Uspesno izbrisan");
                konekcija.Close();
                button1.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }

        private void Periodi_Load(object sender, EventArgs e)
        {
               
            button2.Enabled = false;
            button3.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string b = "Uspeno izmenjeno";
            try
            {
              
                SqlCommand izmeni = new SqlCommand("UPDATE Period SET id=" + textBox1.Text + ",period='" + textBox2.Text + "' WHERE id=" + textBox1.Text, konekcija);
                konekcija.Open();
                izmeni.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("Pogresno unesene vrednosti");
                b = "Neuspesno izmenjeno"; 
            }
            finally
            {
                konekcija.Close();
                MessageBox.Show(b);
            }
        }
    }
}
