﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EiT_B12 //Stefan Rasuo
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        SqlConnection konekcija = new SqlConnection(@"Data Source=DESKTOP-731F4BP\SQLEXPRESS;Initial Catalog=B12;Integrated Security=True");
        private void Form2_Load(object sender, EventArgs e)
        {

            SqlCommand indexi = new SqlCommand("SELECT DISTINCT [Pozorisni_Komad].[KomadID] FROM [Pozorisni_Komad]", konekcija);
            SqlCommand producent = new SqlCommand("SELECT DISTINCT [Producent].[ProducentID] , [Producent].[Ime] FROM [Producent]", konekcija);
            SqlCommand trupa = new SqlCommand("SELECT DISTINCT [Pozorisna_Trupa].[TrupaID] , [Pozorisna_Trupa].[NazivTrupe] FROM [Pozorisna_Trupa]", konekcija);
            konekcija.Open();
            SqlDataReader rdr = indexi.ExecuteReader();
            while (rdr.Read())
            {
                comboBox1.Items.Add(rdr[0].ToString());
            }
            konekcija.Close();

            konekcija.Open();
            SqlDataReader rdr1 = producent.ExecuteReader();
            while (rdr1.Read())
            {
                comboBox2.Items.Add(rdr1[0].ToString() + " " + rdr1[1].ToString());
            }
            konekcija.Close();

            konekcija.Open();
            SqlDataReader rdr2 = trupa.ExecuteReader();
            while (rdr2.Read())
            {
                comboBox3.Items.Add(rdr2[0].ToString() + " " + rdr2[1].ToString());
            }
            konekcija.Close();



        }

        private void button1_Click(object sender, EventArgs e)

        {
            try
            {
                if (radioButton1.Checked == true)
                {
                    SqlCommand upis = new SqlCommand("INSERT INTO Predstava (KomadID , Datum , BrojMesta , CenaKarte , ProducentID , TrupaID) VALUES (" + Convert.ToInt64(comboBox1.Text) + " , '" + textBox1.Text + "' , " + Convert.ToInt32(textBox2.Text) + " , " + Convert.ToInt32(textBox3.Text) + " , '" + Convert.ToInt64(comboBox2.Text.Remove(comboBox2.Text.IndexOf(' '))) + "' , '" + Convert.ToInt64(comboBox3.Text.Remove(comboBox3.Text.IndexOf(' '))) + "' );", konekcija);
                    konekcija.Open();
                    upis.ExecuteNonQuery();
                    MessageBox.Show("Done");
                    konekcija.Close();
                }
                if (radioButton2.Checked == true)
                {
                    if (comboBox1.Text != "" && textBox1.Text != "")
                    {
                        SqlCommand delete = new SqlCommand("DELETE FROM [Predstava] WHERE [Predstava].[KomadID] = " + Convert.ToInt64(comboBox1.Text) + " AND [Predstava].[Datum] = '" + textBox1.Text + "'", konekcija);
                        konekcija.Open();
                        delete.ExecuteNonQuery();
                        MessageBox.Show("Done");
                        konekcija.Close();
                    }
                    if (comboBox1.Text != "" && textBox1.Text == "")
                    {
                        DialogResult dialogResult = MessageBox.Show("Da li zelite da obrisete sve stavke koje sadrze: " + comboBox1.Text + "?", "Upozorenje", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            SqlCommand delete = new SqlCommand("DELETE FROM [Predstava] WHERE [Predstava].[KomadID] = " + Convert.ToInt64(comboBox1.Text) + ";", konekcija);
                            konekcija.Open();
                            delete.ExecuteNonQuery();
                            MessageBox.Show("Done");
                            konekcija.Close();
                        }
                        else if (dialogResult == DialogResult.No)
                        {
                            //nista
                        }
                    }
                    if (comboBox1.Text == "" && textBox1.Text != "")
                    {
                        DialogResult dialogResult1 = MessageBox.Show("Da li zelite da obrisete sve stavke koje sadrze: " + textBox1.Text + "?", "Upozorenje", MessageBoxButtons.YesNo);
                        if (dialogResult1 == DialogResult.Yes)
                        {
                            SqlCommand delete = new SqlCommand("DELETE FROM [Predstava] WHERE [Predstava].[Datum] = '" + textBox1.Text + "';", konekcija);
                            konekcija.Open();
                            delete.ExecuteNonQuery();
                            MessageBox.Show("Done");
                            konekcija.Close();
                        }
                        else if (dialogResult1 == DialogResult.No)
                        {
                            //nista
                        }
                    }
                    else {

                        MessageBox.Show("Invalid");

                    }
                }
            }



            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
               
            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)//Dodaje crtice za format datuma (npr 20171231 = 2017-12-31)
        {
            if (textBox1.TextLength == 4)
            {
                textBox1.Text += "-";
                textBox1.SelectionStart = textBox1.TextLength;//SlectionStart je linija gde se pise tekst (ona uspravna crtica)
            }
            if (textBox1.TextLength == 7)
            {
                textBox1.Text += "-";
                textBox1.SelectionStart = textBox1.TextLength;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void domainUpDown1_SelectedItemChanged(object sender, EventArgs e)
        {

        }
    }
}


