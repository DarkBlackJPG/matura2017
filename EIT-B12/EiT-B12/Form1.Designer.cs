﻿namespace EiT_B12
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.unosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezervacijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.predstaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preglediToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poTrupamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poKomadimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazAltIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unosToolStripMenuItem,
            this.preglediToolStripMenuItem,
            this.krajToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // unosToolStripMenuItem
            // 
            this.unosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rezervacijeToolStripMenuItem,
            this.predstaveToolStripMenuItem});
            this.unosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.unosToolStripMenuItem.Name = "unosToolStripMenuItem";
            this.unosToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.unosToolStripMenuItem.Text = "Unos";
            // 
            // rezervacijeToolStripMenuItem
            // 
            this.rezervacijeToolStripMenuItem.Name = "rezervacijeToolStripMenuItem";
            this.rezervacijeToolStripMenuItem.Size = new System.Drawing.Size(150, 24);
            this.rezervacijeToolStripMenuItem.Text = "Rezervacije";
            // 
            // predstaveToolStripMenuItem
            // 
            this.predstaveToolStripMenuItem.Name = "predstaveToolStripMenuItem";
            this.predstaveToolStripMenuItem.Size = new System.Drawing.Size(150, 24);
            this.predstaveToolStripMenuItem.Text = "Predstave";
            this.predstaveToolStripMenuItem.Click += new System.EventHandler(this.predstaveToolStripMenuItem_Click);
            // 
            // preglediToolStripMenuItem
            // 
            this.preglediToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.poTrupamaToolStripMenuItem,
            this.poKomadimaToolStripMenuItem});
            this.preglediToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.preglediToolStripMenuItem.Name = "preglediToolStripMenuItem";
            this.preglediToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.preglediToolStripMenuItem.Text = "Pregledi";
            // 
            // poTrupamaToolStripMenuItem
            // 
            this.poTrupamaToolStripMenuItem.Name = "poTrupamaToolStripMenuItem";
            this.poTrupamaToolStripMenuItem.Size = new System.Drawing.Size(161, 24);
            this.poTrupamaToolStripMenuItem.Text = "Po trupama";
            // 
            // poKomadimaToolStripMenuItem
            // 
            this.poKomadimaToolStripMenuItem.Name = "poKomadimaToolStripMenuItem";
            this.poKomadimaToolStripMenuItem.Size = new System.Drawing.Size(161, 24);
            this.poKomadimaToolStripMenuItem.Text = "Po komadima";
            this.poKomadimaToolStripMenuItem.Click += new System.EventHandler(this.poKomadimaToolStripMenuItem_Click);
            // 
            // krajToolStripMenuItem
            // 
            this.krajToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.izlazAltIToolStripMenuItem});
            this.krajToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.krajToolStripMenuItem.Name = "krajToolStripMenuItem";
            this.krajToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.krajToolStripMenuItem.Text = "Kraj";
            // 
            // izlazAltIToolStripMenuItem
            // 
            this.izlazAltIToolStripMenuItem.Name = "izlazAltIToolStripMenuItem";
            this.izlazAltIToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.izlazAltIToolStripMenuItem.Text = "Izlaz Alt+I";
            this.izlazAltIToolStripMenuItem.Click += new System.EventHandler(this.izlazAltIToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem unosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preglediToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izlazAltIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rezervacijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem predstaveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem poTrupamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem poKomadimaToolStripMenuItem;
    }
}

