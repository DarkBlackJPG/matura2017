﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EiT_B12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void predstaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 Predstave = new Form2();
            Predstave.Show();
        }

        private void poKomadimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 Komadi = new Form3();
            Komadi.Show();
        }

        private void izlazAltIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
