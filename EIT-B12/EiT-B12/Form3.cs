﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace EiT_B12
{
    
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        SqlConnection konekcija = new SqlConnection(@"Data Source=DESKTOP-731F4BP\SQLEXPRESS;Initial Catalog=B12;Integrated Security=True");
        public void Refresh(string y1 , string m1 , string d1 , string y2, string m2, string d2)
        {
            try {
                SqlCommand selektor = new SqlCommand("SELECT DISTINCT [Pozorisni_Komad].[KomadID] , [Pozorisni_Komad].[Naziv] , [Pozorisni_Komad].[Trajanje] FROM [B12].[dbo].[Pozorisni_Komad] , [B12].[dbo].[Predstava] WHERE [Predstava].[Datum] > '" + y1 + "-" + m1 + "-" + d1 + "' AND [Predstava].[Datum] < '" + y2 + "-" + m2 + "-" + d2 + "'", konekcija);
                konekcija.Open();
                SqlDataAdapter Ad = new SqlDataAdapter(selektor);
                DataSet ds = new DataSet();
                Ad.Fill(ds, "Pozorisni_Komad");
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Pozorisni_Komad";
                konekcija.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                konekcija.Close();
            }
        }
            

        

        private void Form3_Load(object sender, EventArgs e)
        {
           Refresh(textBox1.Text , comboBox1.Text , comboBox2.Text , textBox2.Text , comboBox3.Text , comboBox4.Text);
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            Refresh(textBox1.Text, comboBox1.Text, comboBox2.Text, textBox2.Text, comboBox3.Text, comboBox4.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            SqlCommand selektor = new SqlCommand("SELECT Pozorisna_Trupa.NazivTrupe, SUM(Predstava.CenaKarte * Predstava.BrojMesta) as Prihod FROM Predstava, Pozorisna_Trupa, Pozorisni_Komad  WHERE Predstava.KomadID = '"+ Convert.ToString(e.RowIndex.ToString()) + "' AND Pozorisni_Komad.KomadID = Predstava.KomadID AND Predstava.TrupaID = Pozorisna_Trupa.TrupaID GROUP BY Pozorisna_Trupa.NazivTrupe", konekcija);
            konekcija.Open();
            SqlDataAdapter Ad = new SqlDataAdapter(selektor);
            DataSet ds = new DataSet();
            Ad.Fill(ds, "Pozorisna_Trupa");
            dataGridView2.DataSource = ds;
            dataGridView2.DataMember = "Pozorisna_Trupa";
            konekcija.Close();
            
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
    
}
